from hpgprx import *

import os
import time
import traceback

def processPPDU(runid, session, rx, dbwriter, file, filenum = 0, breakonerror=True):
	print("Processing PPDU: {}".format(file))
	
	src = SignalSources.ComplexFileSignalSource(file)
			
	try:
		result = receiver.rx(src)
	except Exception as e:
		if breakonerror:
			raise e
		else:
			result = HPAVrx.RXResult(src.filename)
			result.error(str(e), trace=traceback.format_exc())

	print(result)
	dbwriter.storeResult(result, runid, filenum)


def processPPDUs(runid, session, rx, dbwriter, directory, breakonerror=False):	
	if not os.path.exists(directory):
		raise FileNotFoundError(directory)
	print("Processing all PPDUs in directory: {}".format(directory))

	for (filenum, sigfile) in utils.sortedGlob(os.path.join(directory, "*.dat")):
		processPPDU(runid, session, rx, dbwriter, sigfile, filenum, breakonerror)
					
		
		
##########################MAIN SCRIPT#################################

inputdirs = [
	"example-data",
]

utils.initResultsDir()													#make sure the results directory exists if this is the first run

dbwriter = DBResultWriter.DBResultWriter(os.path.join(config.resultsdir, config.resultsdb))

runid = int(time.time())											#top level identifier for everything processed in one go, just for convenience if more than one run is in the same DB
with dbwriter:
	for inputdir in inputdirs:
		session = inputdir.split("/")[-1].replace("-PPDUs", "")			#identifier for an individual capture that might be within a run (e.g., a whole directory is a session, with multiple dirs processed in one run)
		receiver = HPAVrx.HPAVRX(sessionname=session)						#the session name becomes the .pcap file name, one can also pass in custom PHY RX params per run if something is known about them apriori
		processPPDUs(runid, session, receiver, dbwriter, inputdir, breakonerror=False)					#process the messages -- can either allow exceptions to stop the run or just be written to the DB