from .utils import *

class SignalSource:
	COMPLEX = 1
	REAL = 2
	REAL_AS_COMPLEX = 3

	def read(self):
		raise NotImplementedError("Base SignalSource class")
	
	def getValueType(self):
		raise NotImplementedError("Base SignalSource class")

class RFileSignalSource(SignalSource):
	def __init__(self, filename):
		self.filename = filename
		debug("R source file: {}".format(filename))
	
	def read(self):
		return loadRFileFloats(self.filename)
	
	def getValueType(self):
		return SignalSource.REAL

class RealsAsComplexFileSignalSource(SignalSource):
	def __init__(self, filename):
		self.filename = filename
		debug("Raw signal file with real signal but in complex: {}".format(filename))
	
	def read(self):
		#if one was going to interpolate or frequency offset the values, it would be done here
		return loadRawComplex(self.filename)
	
	def getValueType(self):
		return SignalSource.REAL_AS_COMPLEX
		
	def rationalInterpolate(vals, interp, decim):
		vi = np.linspace(0, 1, len(vals))
		oi = np.linspace(0, 1, len(vals) * interp / decim)
		return np.interp(oi, vi, vals)

	def frequencyOffset(vals, frequency):
		offsetvals = np.linspace(0, len(vals)-1, len(vals))
		offsetvals = sin(offsetvals)
	
	#handle the capture properties
	def preprocessing(sig):
		sig = sig - np.mean(sig)	#remove dc component
		sig = -sig					#invert due to coupling
		return sig

class ComplexFileSignalSource(SignalSource):
	def __init__(self, filename):
		self.filename = filename
		debug("Raw signal file with complex signal: {}".format(filename))
	
	def read(self):
		#if one was going to interpolate or frequency offset the values, it would be done here
		return loadRawComplex(self.filename)
	
	def getValueType(self):
		return SignalSource.COMPLEX