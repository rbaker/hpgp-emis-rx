import numpy as np

class TurboInterleaver:
	def __init__(self):
		self.params = self.generateInterleavingParams()

	def generateInterleavingParams(self):
		params = {}

		for pbsize in [16, 136, 520]:
			if pbsize == 16:
				S = [54, 23, 61, 12, 35, 2, 40, 25]
				N = 8
				M = 8
				L = 64
			elif pbsize == 136:
				S = [
					369, 235, 338, 436, 169, 200, 397, 59,
					298, 20, 265, 429, 294, 466, 16, 48,
					525, 461, 187, 86, 216, 387, 41, 142,
					247, 314, 79, 486, 512, 103, 476, 345, 
					4, 105
					]
				N = 34
				M = 16
				L = 544
			elif pbsize == 520:
				S = [
					1558, 1239, 315, 1114, 437, 956, 871, 790, 
					833, 1152, 147, 506, 589, 388, 1584, 265, 
					981, 220, 1183, 102, 1258, 1019, 1296, 737, 
					694, 1495, 612, 453, 1049, 1450, 531, 47, 
					1368, 645, 166, 322, 1323, 1404, 0, 881
					]
				N = 40
				M = 52
				L = 2080
			else:
				raise ValueError("Invalid pbsize")

			I = self.turboInterleaverMapping(S, N, L)

			params[pbsize] = (S, N, M, L, I)

		return params

	def turboInterleaverMapping(self, S, N, L):		
			#I = np.zeroes((pbsize*8)/2)
			#for i in range((pbsize*8)/2):

			I = [ (S[x % N] - int(x / N)*N+L) % L for x in range(L) ]

			return I

	def interleave(self, pbsizebits, mapping, data):
		#intdata = np.zeros(pbsizebits)
		intdata = [(None, None) for i in range(pbsizebits)]

		for i in range(int(pbsizebits/2)):
			if i % 2 == 0:
				intdata[2*i] = data[2*mapping[i]+1]
				intdata[2*i+1] = data[2*mapping[i]]
			else:
				intdata[2*i] = data[2*mapping[i]]
				intdata[2*i+1] = data[2*mapping[i]+1]
		return list(intdata)

	def deinterleave(self, pbsizebits, mapping, intdata):
		#data = np.zeros(pbsizebits)
		data = [(None,None) for i in range(pbsizebits)]

		for i in range(int(pbsizebits/2)):
			if i % 2 == 0:
				data[2*mapping[i]+1] = intdata[2*i]
				data[2*mapping[i]] = intdata[2*i+1]
			else:
				data[2*mapping[i]] = intdata[2*i]
				data[2*mapping[i]+1] = intdata[2*i+1]
		return list(data)


##for size in [16, 136, 520]:
#for size in [16]:
#	#mapping = turboInterleaverMapping(None, size)
#	#print(len(mapping))
#	#print(mapping)
#	#print((min(mapping), max(mapping)))
#	#print
#	##from matplotlib.pyplot import show, plot
#	##show(plot(sorted(mapping)))
#
#
#	pbsize = size
#	pbsizebits = pbsize * 8
#	
#	data = [x for x in range(pbsizebits)]
#
#	genparams = generateInterleavingParams(None)
#	#mapping = turboInterleaverMapping(None, S, N, L)
#	mapping = genparams[pbsize][4]
#	print(mapping)
#	intdata = interleave(None, pbsizebits, mapping, data)
#	redata = deinterleave(None, pbsizebits, mapping, intdata)
#	#print(intdata)
#	#print
#	print(redata)