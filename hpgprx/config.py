SIG_DEBUG = False			#in enabled, will plot various stages of the demodulation process

dbworkerfinalisetimeout = 5.0

#PHY layer properties
turboiterations = [0, 1, 3, 5]

resultsdir = "results"
resultsdb = "decoded.sqlite3"
