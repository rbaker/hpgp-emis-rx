import math
from .utils import *

usableBW = (0, 1500)
#usableBW = (650, 1150)
#usableBW = (550, 1150)
print("WARN: Using limited bandwidth mode")

scnums = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153]
#scfreqs = c(1.953125, 2.148438, 2.34375, 2.539063, 2.734375, 2.929688, 3.125, 3.320313, 3.515625, 3.710938, 3.90625, 4.101563, 4.296875, 4.492188, 4.6875, 4.882813, 5.078125, 5.273438, 5.46875, 5.664063, 5.859375, 6.054688, 6.25, 6.445313, 6.640625, 6.835938, 7.03125, 7.226563, 7.421875, 7.617188, 7.8125, 8.007813, 8.203125, 8.398438, 8.59375, 8.789063, 8.984375, 9.179688, 9.375, 9.570313, 9.765625, 9.960938, 10.15625, 10.351563, 10.546875, 10.74219, 10.9375, 11.13281, 11.32813, 11.52344, 11.71875, 11.91406, 12.10938, 12.30469, 12.5, 12.69531, 12.89063, 13.08594, 13.28125, 13.47656, 13.67188, 13.86719, 14.0625, 14.25781, 14.45313, 14.64844, 14.84375, 15.03906, 15.23438, 15.42969, 15.625, 15.82031, 16.01563, 16.21094, 16.40625, 16.60156, 16.79688, 16.99219, 17.1875, 17.38281, 17.57813, 17.77344, 17.96875, 18.16406, 18.35938, 18.55469, 18.75, 18.94531, 19.14063, 19.33594, 19.53125, 19.72656, 19.92188, 20.11719, 20.3125, 20.50781, 20.70313, 20.89844, 21.09375, 21.28906, 21.48438, 21.67969, 21.875, 22.07031, 22.26563, 22.46094, 22.65625, 22.85156, 23.04688, 23.24219, 23.4375, 23.63281, 23.82813, 24.02344, 24.21875, 24.41406, 24.60938, 24.80469, 25, 25.19531, 25.39063, 25.58594, 25.78125, 25.97656, 26.17188, 26.36719, 26.5625, 26.75781, 26.95313, 27.14844, 27.34375, 27.53906, 27.73438, 27.92969, 28.125, 28.32031, 28.51563, 28.71094, 28.90625, 29.10156, 29.29688, 29.49219, 29.6875, 29.88281)
scphasenums = [15, 1, 0, 11, 12, 8, 15, 14, 5, 14, 13, 10, 4, 0, 0, 15, 15, 14, 13, 12, 11, 9, 7, 6, 3, 1, 15, 12, 9, 6, 3, 15, 12, 8, 4, 0, 11, 7, 2, 13, 8, 3, 13, 7, 2, 11, 5, 15, 8, 1, 10, 3, 11, 4, 12, 4, 12, 3, 11, 2, 9, 0, 7, 13, 3, 10, 15, 5, 11, 0, 5, 10, 15, 3, 8, 12, 0, 4, 7, 11, 14, 1, 4, 7, 9, 11, 14, 15, 1, 3, 4, 5, 6, 7, 7, 8, 8, 3, 15, 4, 10, 13, 15, 6, 11, 3, 2, 12, 13, 9, 7, 7, 8, 11, 12, 8, 1, 7, 8, 1, 7, 0, 12, 13, 0, 1, 1, 15, 11, 12, 6, 5, 13, 2, 9, 11, 14, 4, 9, 5, 8, 8, 9, 9]
scmasks = [True, False, False, False, False, False, False, False, True, True, True, True, False, False, False, False, False, True, True, False, False, False, False, False, False, False, True, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, True, False, False, False, False, False, False, False, False, False, False, False, False, False, True, True, True, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, True, True, True, True, True, True, True, True, True, True, True]

SYNCP_LEN = 384
HALF_SYNCP_LEN = 192

HS_ROBO_COPIES = 2
STD_ROBO_COPIES = 4
MINI_ROBO_COPIES = 5

PREAMBLE_TONE_MASK = []
for i in range(0, 192):
	active = False
	if i in scnums and not scmasks[i-10]:
		active = True
	PREAMBLE_TONE_MASK.append(active)
#print(PREAMBLE_TONE_MASK)



def isUnmasked(subcar):
	if (0 <= subcar and subcar <= 70):  return False 
	if (71 <= subcar and subcar <= 73):  return False 
	if (74 <= subcar and subcar <= 85):  return False 
	if (86 <= subcar and subcar <= 139):  return True 
	if (140 <= subcar and subcar <= 167):  return False 
	if (168 <= subcar and subcar <= 214):  return True 
	if (215 <= subcar and subcar <= 225):  return False 
	if (226 <= subcar and subcar <= 282):  return True 
	if (283 <= subcar and subcar <= 302):  return False 
	if (303 <= subcar and subcar <= 409):  return True 
	if (410 <= subcar and subcar <= 419):  return False 
	if (420 <= subcar and subcar <= 569):  return True 
	if (570 <= subcar and subcar <= 591):  return False 
	if (592 <= subcar and subcar <= 736):  return True 
	if (737 <= subcar and subcar <= 748):  return False 
	if (749 <= subcar and subcar <= 856):  return True 
	if (857 <= subcar and subcar <= 882):  return False 
	if (883 <= subcar and subcar <= 1015):  return True 
	if (1016 <= subcar and subcar <= 1027):  return False 
	if (1028 <= subcar and subcar <= 1143):  return True 
	if (1144 <= subcar and subcar <= 1535):  return False 
	else: raise Exception("Unrecognised subcarrier number") 


mainscnums = []
mainscphasenums = []
with open(package_directory + "/ref-data/HPAVMainSubcarrierPhases.csv") as inf:
	inf.readline()
	for l in inf:
		(n, p) = l.strip().split(",")
		mainscnums.append(int(n))
		mainscphasenums.append(int(p))
MAIN_TONE_MASK = []
m2 = []
for i in range(0, 1536):
	MAIN_TONE_MASK.append(i in mainscnums and isUnmasked(i))
	m2.append(1 if i in mainscnums and isUnmasked(i) else 0)
#print(MAIN_TONE_MASK)
#plotSig(m2)

EXPECTED_SUBCARRIER_COUNT = 917	#use something like the following once a main tone mask is defined:  len([x for x in PREAMBLE_TONE_MASK if x])


class PreambleTemplate:
	@staticmethod
	def scval(c, n, p):
		return math.cos(((2 * math.pi * c * n)/384)+p)
	
	@staticmethod
	def syncpval(n):
		total = 0
		for i in range(0, 192):
			if PREAMBLE_TONE_MASK[i]:
				total += PreambleTemplate.scval(i, n, scphasenums[i-10] * (math.pi/8))
		
		return total
	
	@staticmethod
	def syncp():
		vals = []
		for i in range(0, 384):
			vals.append(PreambleTemplate.syncpval(i))
		
		return vals