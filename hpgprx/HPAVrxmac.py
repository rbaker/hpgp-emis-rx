from .config import resultsdir
from .utils import *
#import MPDU
from .MPDU import *

from .pcap import *

import os


class HPAVRXMAC:
	def __init__(self, sessionname):
		self.NMK = None
		self.NEK = None
		self.pcap = Pcap(os.path.join(config.resultsdir, sessionname + ".pcap"))
		self.follower = MACFollower()
	
	def handle(self, ppdu):
		if ppdu.avfc.delimtype != 1:
			return (None, None)
		
		keys = []
		
		#print details about the phy block
		print("SSN: {}".format(ppdu.payload.ssn))
		print("MFBO: {}".format(ppdu.payload.mfbo))
		print("VPBF: {}".format(ppdu.payload.vpbf))
		print("MMQF: {}".format(ppdu.payload.mmqf))
		print("MFBF: {}".format(ppdu.payload.mfbf))
		print("OPSF: {}".format(ppdu.payload.opsf))
		print("RSVD: {}".format(ppdu.payload.rsvd))
		
		#see whether it's encrypted
		eks = ppdu.avfc.variant.fields["eks"]
		eks = int.from_bytes(eks, byteorder='little', signed=False)
		
		if eks == 15:		#unencrypted
			ppdubits = ppdu.payload.data
			mpdu = MACFrame(ppdubits)
			
			#dump it to a .pcap file
			self.dumpToPcap(bitsToBytes(ppdubits[6*8:]))

			#if it is a management message
			if mpdu.mftype == 3:				
				#if we find a SLAC CNF message then update the NMK
				if int.from_bytes(mpdu.mm.mmtype, byteorder='little', signed=False) == 0x607D:		#according to the spec this is CM_VALIDATE.CNF, but it appears to actually be CM_SLAC_MATCH.CNF
					if mpdu.mm.mme.nmk is not None:
						self.NMK = mpdu.mm.mme.nmk
						debug("Retrieved NMK: {}".format(mpdu.mm.mme.nmk))
						keys.append(("NMK", mpdu.mm.mme.nmk))

				#if we find an Authentication message then update the NEK
				if int.from_bytes(mpdu.mm.mmtype, byteorder='little', signed=False) == 0x6006:
					#self.NMK = binascii.unhexlify("edb036a17f1f0d33d5d669c934ac4328")					#TODO: this is temporary, need to find a way to pass it in for single runs or not for whole batches
					if self.NMK is not None:
						encdata = mpdu.mm.mme
						decdata = self.decryptEncryptedPayload(self.NMK, encdata)					

						#remove the random filler
						rflen = decdata[-1]
						decdata = decdata[rflen:]
						debug("Removed encrypted payload random filler of {} bytes".format(rflen))

						#save to pcap
						self.dumpToPcap(decdata)
						
						msg = ManagementMessage(decdata)
						mpdu.mm.encmm = msg

						if int.from_bytes(msg.mmtype, byteorder='little', signed=False) == 0x600D:
							self.NEK = msg.mme.enckey
							debug("Retrieved NEK: {} ({})".format(msg.mme.enckey, binascii.hexlify(msg.mme.enckey)))
							keys.append(("NEK", msg.mme.enckey))
						
						#merge the generic values fields from the encapsulate message and the main one
						for v in msg.values:
							mpdu.mm.values["CM_Enc_Pay." + v] = msg.values[v]
						
		
		else:
			#encrypted
			mpdu = None
			
			#if we have an NEK then assume it is the right one and try to decrypt the message
			if self.NEK is not None:
			
				####################
				#	AES operates with the opposite endianness to HPGP, so everything needs to swap here
				####################
				
				nek = bytes(bitsToReverseBytes(bytesToBits(self.NEK)))
				#nek = bytes(bitsToReverseBytes(bytesToBits(binascii.unhexlify("734a5136e018d31965bc0074383d10e2"))))	#endianness reversed

				iv = bitsToBytes(ppdu.avfc.variant.rawbits) + bytearray("\x00".encode("UTF-8")) + bitsToBytes(ppdu.payload.rawbits[:24])
				iv = bytes(bitsToReverseBytes(bytesToBits(iv)))	#endianness swapped
				
				#print("Key: {}".format(binascii.hexlify(nek)))
				#print("IV: {}".format(iv))
				#print("IV: {}".format(binascii.hexlify(iv)))

				ppdubytes = bitsToBytes(ppdu.payload.data)
				ppdubytes = bytes(bitsToReverseBytes(bytesToBits(ppdubytes)))	#endianness swapped

				#print("CT: {}".format(binascii.hexlify(ppdubytes)))

				ppdubytes = ppdubytes[:int(len(ppdubytes)/16)*16]		#round to a multiple of 16 bytes		#TODO: fix somehow...?

				#print("PPDU Len: {}".format(len(ppdubytes)))
				#print("PPDU Bytes: {}".format(binascii.hexlify(ppdubytes)))

				pt = self.decryptEncryptedMessage(nek, iv, ppdubytes)
				pt = bytes(bitsToReverseBytes(bytesToBits(pt)))

				#update the mac follower with the plaintext
				print("Adding to frame: {} {} {} {} {}".format(bytes(ppdu.avfc.variant.fields["dtei"]), ppdu.payload.ssn, ppdu.payload.mfbf, ppdu.payload.mfbo, pt))
				frame = self.follower.rxPB(pt, bytes(ppdu.avfc.variant.fields["dtei"]), ppdu.payload.ssn, ppdu.payload.mfbf, ppdu.payload.mfbo)
				if frame is not None:
					mf = MACFrame(bytesToBits(frame))
					if mf.mftype in [1, 2]:
						self.dumpToPcap(mf.macpayload)
					elif mf.mftype == 3:
						self.dumpToPcap(mf.mm.rawbytes)
					mpdu = mf
		
		
		return (mpdu, keys)
	
	def decryptEncryptedPayload(self, key, payload):
		print("PEKS: {}".format(payload.peks))
		print("AVLN Status: {}".format(payload.avlns))
		print("PID: {}".format(payload.pid))
		print("PRN: {}".format(payload.prn))
		print("PMN: {}".format(payload.pmn))
		print("IV: {}".format(payload.iv))
		print("Len: {}".format(payload.len))
		print("Length: {}".format(payload.length))
		print("Encrypted Data: {}".format(payload.encdata))
		
		key = bytearray(key)
		
		from Crypto.Cipher import AES
		cipher = AES.new(bytes(key), AES.MODE_CBC, bytes(payload.iv))
		pt = cipher.decrypt(bytes(payload.encdata))
		print("Plaintext: {}".format(pt))
		
		#print(bitsToBytes(ext.get(15 * 8)))
		#print("Bits {} of {} ({} of {})".format(ext.i, ext.max, ext.i/8, ext.max/8))
		
		#encmsg = bytes("\x00\x00".encode("utf-8")) + pt											#TODO: losing two bytes off the front, for some unexplained reason...
		encmsg = pt
		
		return encmsg
	
	def dumpToPcap(self, decdata):		
		#import pcap
		#p = pcap.Pcap("/sd/Oxford/Oxford CS Research/Powerline/gr-homeplug-emis/test.pcap")
		#p.write(decdata)
		#p.close()
		self.pcap.write(decdata)
		
	
	def decryptEncryptedMessage(self, key, iv, msg):	
		key = bytearray(key)
		iv = bytearray(iv)
		
		from Crypto.Cipher import AES
		cipher = AES.new(bytes(key), AES.MODE_CBC, bytes(iv))
		pt = cipher.decrypt(bytes(msg))
		print("Plaintext: {}".format(pt))
		
		return pt
	
	def testPossibleEtherframe(self, typebytes):
		ethertypes = [
			([0x08, 0x06],	"Address Resolution Protocol (ARP)"),
			([0x08, 0x00],	"Internet Protocol version 4 (IPv4)"),
			([0x08, 0x42],	"Wake-on-LAN[9]"),
			([0x22, 0xF3],	"IETF TRILL Protocol"),
			([0x22, 0xEA],	"Stream Reservation Protocol"),
			([0x60, 0x03],	"DECnet Phase IV"),
			([0x80, 0x35],	"Reverse Address Resolution Protocol"),
			([0x80, 0x9B],	"AppleTalk (Ethertalk)"),
			([0x80, 0xF3],	"AppleTalk Address Resolution Protocol (AARP)"),
			([0x81, 0x00],	"VLAN-tagged frame (IEEE 802.1Q) and Shortest Path Bridging IEEE 802.1aq with NNI compatibility[10]"),
			([0x81, 0x37],	"IPX"),
			([0x82, 0x04],	"QNX Qnet"),
			([0x86, 0xDD],	"Internet Protocol Version 6 (IPv6)"),
			([0x88, 0x08],	"Ethernet flow control"),
			([0x88, 0x09],	"Ethernet Slow Protocols[11]"),
			([0x88, 0x19],	"CobraNet"),
			([0x88, 0x47],	"MPLS unicast"),
			([0x88, 0x48],	"MPLS multicast"),
			([0x88, 0x63],	"PPPoE Discovery Stage"),
			([0x88, 0x64],	"PPPoE Session Stage"),
			([0x88, 0x6D],	"Intel Advanced Networking Services [12]"),
			([0x88, 0x70],	"Jumbo Frames (Obsoleted draft-ietf-isis-ext-eth-01)"),
			([0x88, 0x7B],	"HomePlug 1.0 MME"),
			([0x88, 0x8E],	"EAP over LAN (IEEE 802.1X)"),
			([0x88, 0x92],	"PROFINET Protocol"),
			([0x88, 0x9A],	"HyperSCSI (SCSI over Ethernet)"),
			([0x88, 0xA2],	"ATA over Ethernet"),
			([0x88, 0xA4],	"EtherCAT Protocol"),
			([0x88, 0xA8],	"Provider Bridging (IEEE 802.1ad) & Shortest Path Bridging IEEE 802.1aq[10]"),
			([0x88, 0xAB],	"Ethernet Powerlink[citation needed]"),
			([0x88, 0xB8],	"GOOSE (Generic Object Oriented Substation event)"),
			([0x88, 0xB9],	"GSE (Generic Substation Events) Management Services"),
			([0x88, 0xBA],	"SV (Sampled Value Transmission)"),
			([0x88, 0xCC],	"Link Layer Discovery Protocol (LLDP)"),
			([0x88, 0xCD],	"SERCOS III"),
			([0x88, 0xDC],	"WSMP, WAVE Short Message Protocol"),
			([0x88, 0xE1],	"HomePlug AV MME[citation needed]"),
			([0x88, 0xE3],	"Media Redundancy Protocol (IEC62439-2)"),
			([0x88, 0xE5],	"MAC security (IEEE 802.1AE)"),
			([0x88, 0xE7],	"Provider Backbone Bridges (PBB) (IEEE 802.1ah)"),
			([0x88, 0xF7],	"Precision Time Protocol (PTP) over Ethernet (IEEE 1588)"),
			([0x88, 0xF8],	"NC-SI"),
			([0x88, 0xFB],	"Parallel Redundancy Protocol (PRP)"),
			([0x89, 0x02],	"IEEE 802.1ag Connectivity Fault Management (CFM) Protocol / ITU-T Recommendation Y.1731 (OAM)"),
			([0x89, 0x06],	"Fibre Channel over Ethernet (FCoE)"),
			([0x89, 0x14],	"FCoE Initialization Protocol"),
			([0x89, 0x15],	"RDMA over Converged Ethernet (RoCE)"),
			([0x89, 0x1D],	"TTEthernet Protocol Control Frame (TTE)"),
			([0x89, 0x2F],	"High-availability Seamless Redundancy (HSR)"),
			([0x90, 0x00],	"Ethernet Configuration Testing Protocol[13]"),
			([0x91, 0x00],	"VLAN-tagged (IEEE 802.1Q) frame with double tagging")
		]
		
		for i in range(len(ethertypes)):
			#print("Ethertype: {}".format(bytearray(ethertypes[i][0])))
			if typebytes == bytearray(ethertypes[i][0]):
				return True
			
		return False



class MACFollower:
	def __init__(self):
		self.streams = {}	#TODO: only grouped by DTEI at the moment
	
	def rxPB(self, bys, dtei, ssn, mfbf, mfbo):
		#which stream does it relate to
		if dtei not in self.streams:
			self.streams[dtei] = bytearray(0)
		stream = self.streams[dtei]
		
		frame = None
		
		if mfbf == 1:	#there is a mac frame boundary
			#add all remaining bytes to the stream
			stream += bys[:mfbo]
			
			#emit a macframe
			frame = bytearray(stream)		#create a copy
			print("Frame emitted: {}".format(frame))
			
			#begin with the new bytes
			stream = bys[mfbo:]
			self.streams[dtei] = stream
		
		#add the bytes to the stream		
		stream += bys
		
		print("Stream {} length: {}".format(dtei, len(stream)))
		
		return frame