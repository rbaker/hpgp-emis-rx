# import math
# import numpy as np
# from matplotlib.pyplot import plot, show
# from scipy.signal import hilbert
# import struct
# import glob

from .SignalSources import *
from .RXEnv import *
#import PPDU
from .PPDU import *
#import MPDU
from .MPDU import *
from .utils import *
from .config import SIG_DEBUG, turboiterations
from .HPAVEnv import *
from .ConvolutionalCodes import *
from .Turbo import *

from scipy.signal import fftconvolve


class HPAVRXPHY:
	preamblelen = 9*384
	hp1fclen = 4*(246+384)+372
	hpavfclen = 1374 + 3072
	paysym1len = 567 + 3072
	paysym2len = 567 + 3072
	fcguard = 1374
	paysymxguard = 567
	paysymxlen = paysymxguard + 3072

	def __init__(self, rxenv = RXEnv()):
		self.rxenv = rxenv
		self.precorr = PreambleCorrelator()
		self.cpoestim = CPOEstimator()
		self.cpocorrect = CPOCorrector()
		self.chanest = ChannelEstimator()
		self.scoest = SCOEstimator()
		self.cphandler = CyclicPrefixHandler()
		self.demod = Demodulator()
		self.hpavpostproc = HPAVPostProcessor()
		self.chandeinter = ChannelDeinterleaver()
		self.paypostproc = PayloadPostProcessor()
		self.decoder = FECDecoder()

	def rx(self, src):
		self.rxenv.debugvals["filename"] = src.filename
		import time
		self.rxenv.debugvals["runid"] = int(time.time() % 1000000)

		self.rxenv.residualcpos = [0]			#reset the cpos for this ppdu (with 0 in the list to slightly dampen early updates)

		sig = src.read()

		ppdustart = self.precorr.findPreamble(sig)
		debug("PPDU Start: {}".format(ppdustart))

		if ppdustart is None:
			debug("No PPDU start found, discarding")
			raise ValueError("No PPDU start found")

		if len(sig) - ppdustart < PPDU.preamblelen + PPDU.hp1fclen + PPDU.hpavfclen:		#the minimum before it becomes any use at all
			debug("Signal section too short for PPDU")
			raise ValueError("Signal section too short for PPDU")

		#set the useful bandwidth parameters
		#MOVED TO HPAVEnv.py

		#pad the front if part of the preamble was missed
		if ppdustart < 0:
			debug("PPDU start negative, padding front with zeroes")
			sig = [0] * (ppdustart * -1) + sig
			ppdustart = 0

		#convert to complex signal (if necessary)
		if src.getValueType() == SignalSource.REAL:
			debug("Converting signal to complex")
			sig = hilbert(sig)
		if src.getValueType() == SignalSource.REAL_AS_COMPLEX:
			debug("Converting real-as-complex signal to complex")

			#as a bit of a hack, pad with zeroes to a power of 2, shouldn't make much difference to the result
			debug("WARN: Padding with zeroes, for hilbert transform")
			pad = 2 ** math.ceil(np.log(len(sig)) / np.log(2)) - len(sig)
			sig = sig + [np.complex(0,0)] * pad

			sig = hilbert(np.real(sig))

		#remove any leading signal
		sig = sig[ppdustart:]

		#get the preamble section
		preamble = sig[:self.preamblelen]

		#estimate cpo for the signal
		cpo = self.cpoestim.estimateCarrierPhaseOffsetFromPreamble(preamble)
		self.rxenv.updateCPO(cpo)
		debug("CPO Estimate: {}".format(cpo))
		self.rxenv.debugvals["cpoinit"] = cpo

		#correct for the cpo and reselect the preamble
		sig = self.cpocorrect.correctCarrierPhaseOffset(sig, self.rxenv.cpoestimate, HALF_SYNCP_LEN)		#start in the same place as estimation for correction
		preamble = sig[:self.preamblelen]		#TODO: is this necessary?

		prechannel = self.chanest.estimateChannel(preamble)
		sco = self.scoest.estimateSCOFromChannel(prechannel)
		othersco = self.scoest.estimateSCOByLOBF(prechannel)
		debug("\nSCO Compare: orig={} vs new={}\n".format(sco, othersco))
		self.rxenv.updateSCO(sco)
		debug("SCO Estimate: {}".format(sco))

		#generate the channel estimate for the 3072 point FFT
		channel = self.chanest.generateFullChannelEstimate(prechannel)
		self.rxenv.updateChannel(channel)

		#separate the FC signal
		hp1fc = sig[self.preamblelen:self.preamblelen + self.hp1fclen]
		hpavfc = sig[self.preamblelen + self.hp1fclen:self.preamblelen + self.hp1fclen + self.hpavfclen]

		#ignore the HP1FC for now

		#process the HPAVFC
		#(turbo decoding is very expensive, so attempt to avoid it but then apply it successively more if we fail)
		for turboits in config.turboiterations:
			hpavfcbits = self.processHPAVFC(hpavfc, turboits)
			avfc = AVFrameControl(hpavfcbits)

			if avfc.isChecksumOk():
				break
			else:
				debug("AVFC checksum failed with {} turbo iterations".format(turboits))

		#check the final HPAVFC checksum
		if not avfc.isChecksumOk():
			warn("Final AV Frame Control Checksum FAILED")
		else:
			debug("Final AV Frame Control Checksum OK :)")

		#extract values from the HPAVFC
		debug(avfc)

		#check whether to decode the payload
		symcount = avfc.getSymbolCount()
		robomode = avfc.getRoboMode()

		debug("Symbols: {}".format(symcount))
		debug("ROBO Mode: {}".format(robomode))

		if symcount == -2:
			debug("Ignored PPDU type, not proceeding with payload")
			ppdu = PPDU(avfc)
			return ppdu

		if symcount == -1:
			debug("Invalid symbol count, not proceeding with payload")
			raise ValueError("Unrecognised FL_AV, not proceeding with payload")
			#result.error("Unrecognised FL_AV, not proceeding with payload", self.rxenv.debugvals)
			#return result

		#process the payload symbols
		payloadstart = self.preamblelen + self.hp1fclen + self.hpavfclen
		offset = 0
		paysymspairs = []		#will become a list of lists of pairs, one list per symbol
		for i in range(symcount):
			gi = self.getGuardInterval(robomode, i)
			debug("Sym {} GI: {}".format(i, gi))
			if self.morePayload(payloadstart, offset, i, gi, len(sig)):
				paysym = sig[(payloadstart + offset):(payloadstart + offset + self.paySymLen(i, gi))]
				offset += self.paySymLen(i, gi)
				sympairs = self.processPayloadSymbol(paysym, i, gi)
				paysymspairs.append(sympairs)

		#note the remainder
		remainder = sig[payloadstart + offset:len(sig)]
		debug("Remainder after PPDU extraction: {}".format(len(remainder)))

		#process the full payload
		#(turbo decoding is very expensive, so attempt to avoid it but then apply it successively more if we fail)
		for turboits in config.turboiterations:
			#process the payload bits
			payloadbits = self.processCompletePayload(paysymspairs, robomode, turboits)
			payload = Payload(avfc.delimtype, payloadbits)
			if payload.isChecksumOk():
				break
			else:
				debug("Payload checksum failed with {} turbo iterations".format(turboits))

		#report the final payload checksum status
		if not payload.isChecksumOk():
			warn("Final Payload Checksum FAILED")
		else:
			debug("Final Payload Checksum OK :D")

		#package up the result
		ppdu = PPDU(avfc)
		ppdu.addPayload(payload)

		return ppdu

	def processHPAVFC(self, hpavfc, turboiterations = 0):
		self.cphandler.checkCyclicPrefix(hpavfc, HPAVRXPHY.fcguard)
		(cp, sig) = self.cphandler.removeCyclicPrefix(hpavfc, HPAVRXPHY.fcguard)
		hpavfc = self.cphandler.restoreCyclicPrefix(cp, sig)

		pairs = self.demod.demodulateSymbol(hpavfc, self.rxenv, Demodulator.SECTION_HPAVFC)

		#undiversify
		interbits = self.hpavpostproc.reverseFCDiversityAveragingReplicaSoftBits(pairs)
		debug("Interleaved bits:\n{}".format(interbits))

		#channel deinterleave
		(infobits, paritybits) = self.chandeinter.deinterleave(interbits, 128, 128, 4, 16)
		debug("Info bits:\n{}".format(infobits))
		debug("Parity bits:\n{}".format(paritybits))

		#perform the error correction
		if turboiterations > 0:
			bits = self.decoder.turboDecode(infobits, paritybits, 16, turboiterations)
		else:
			bits = (infobits > 0).astype(int)

		return bits

	def processPayloadSymbol(self, paysym, paysymi, gi):
		debug("Processing payload symbol {}".format(paysymi))

		self.cphandler.checkCyclicPrefix(paysym, gi)				#TODO: do something with this value
		(cp, sig) = self.cphandler.removeCyclicPrefix(paysym, gi)

		residualcpoestim = self.cpoestim.estimateCarrierPhaseOffsetFromCP(cp, sig)
		self.rxenv.residualcpos.append(residualcpoestim)
		self.rxenv.debugvals["residcpo" + str(paysymi)] = residualcpoestim
		currentposition = self.preamblelen + self.hp1fclen + self.hpavfclen
		if paysymi == 1:
			currentposition += self.paysym1len
		elif paysymi == 2:
			currentposition += self.paysym1len + self.paysym2len
		elif paysymi > 2:
			currentposition += self.paysym1len + self.paysym2len + (self.paysymxlen * (paysymi-2))

		paysym = self.cpocorrect.correctCarrierPhaseOffset(paysym, np.mean(self.rxenv.residualcpos), currentposition)
		(cp, sig) = self.cphandler.removeCyclicPrefix(paysym, gi)

		paysym = self.cphandler.restoreCyclicPrefix(cp, sig)
		sympairs = self.demod.demodulateSymbol(paysym, self.rxenv, Demodulator.SECTION_PAYLOAD)

		return sympairs

	def processCompletePayload(self, paysymspairs, robomode, turboiterations = 0):
		debug("Processing payload bits")

		#get params for this PPDU
		(robocopies, pbsize) = self.getRoboParams(robomode)
		(chanintinfosize, chanintparitysize, chanintoffset, chanintstepsize) = self.getChannelInterleaverParams(pbsize)

		#combine the payload symbol pairs
		payloadpairs = []
		for sympairs in paysymspairs:
			#drop any unused subcarriers
			dropscs = self.paypostproc.calcSubcarriersToDrop(EXPECTED_SUBCARRIER_COUNT, robocopies)
			sympairs = sympairs[:-dropscs]

			payloadpairs += sympairs

		#unpair the pairs
		robobits = []
		for p in payloadpairs:
			robobits.append(p[0])
			robobits.append(p[1])

		debug("Number of ROBO bits: {}".format(len(robobits)))

		#reverse the ROBO interleaving (as this is HPGP, it's all ROBO)
		rawbits = self.paypostproc.roboDeinterleaveAveragingSoftBits(robobits, robocopies, pbsize)

		debug("Number of raw bits: {}".format(len(rawbits)))

		#channel deinterleave
		(infobits, paritybits) = self.chandeinter.deinterleave(rawbits, chanintinfosize, chanintparitysize, chanintstepsize, chanintoffset)
		#debug("Info bits:\n{}".format(infobits))
		#debug("Parity bits:\n{}".format(paritybits))

		#perform the error correction
		if turboiterations > 0:
			decodedbits = self.decoder.turboDecode(infobits, paritybits, pbsize, turboiterations)

			#debug("PAYLOADCOMPARE")
			#self.decoder.compareDecode(decodedbits, infobits)

			#unscramble the decoded bits
			bits = self.paypostproc.unscramble(decodedbits)
		else:
			#flatten the bits (bceause we didn't do it in turbo)
			infobits = (np.array(infobits) > 0).astype(int)

			#unscramble just the plain bits without FEC
			bits = self.paypostproc.unscramble(infobits)

		debug("Payload length: {}".format(len(bits)))

		return bits

	def morePayload(self, payloadstart, offset, paysymi, gi, siglen):
		if paysymi >= 0:
			return payloadstart + offset + self.paySymLen(paysymi, gi) < siglen
		else:
			raise ValueError("Unrecognised payload symbol index: {}".format(i))

	def paySymLen(self, paysymi, gi):
		if paysymi == 0:
			return HPAVRXPHY.paysym1len
		elif paysymi == 1:
			return HPAVRXPHY.paysym2len
		elif paysymi > 1:
			return 3072 + gi
		else:
			raise ValueError("Unrecognised payload symbol index: {}".format(i))

	def getGuardInterval(self, robomode, paysymi):
		return 567 if robomode == 2 or paysymi < 2 else 417		#first two syms are 567 either way, then HS_ROBO and ROBO switch

	def getRoboParams(self, robomode):
		if robomode == 0:
			params = (STD_ROBO_COPIES, 520)
		elif robomode == 1:
			params = (HS_ROBO_COPIES, 520)
		elif robomode == 2:
			params = (MINI_ROBO_COPIES, 136)

		else:
			raise ValueError("Unrecognised robomode {}".format(robomode))

		return params

	def getChannelInterleaverParams(self, pbsize):
		if pbsize == 136:
			params = (1088, 1088, 136, 16)
		elif pbsize == 520:
			params = (4160, 4160, 520, 16)
		else:
			raise ValueError("Unrecognised pbsize {}".format(pbsize))

		return params

####
class PreambleCorrelator:
	def __init__(self):
		self.template = self.loadPreambleTemplate(package_directory + "/ref-data/HPAV1.1HybridPreamble.float")
		self.peakthreshold = 5 #10 was suitable for whole packet length, but not with shortened value

	def loadPreambleTemplate(self, filename):
		return loadRFileFloats(filename)

	def findPreamble(self, sig):
		#cor = np.correlate(sig, self.template, mode="full") / len(self.template)		#this is convenient but far slower than the scipy way (circular conv is fine here and so is the import time for scipy)
		cor = fftconvolve(sig, self.template[::-1], mode='full') / len(self.template)

		peaki = np.argmax(cor)
		if (cor[peaki] < np.std(cor) * self.peakthreshold):		#heuristic to exclude peaks that don't stand out massively from the background
			debug("Maximum preamble correlation peak: {}".format(cor[peaki]))
			return None

		#find start from peak
		start = peaki - 3072	#8 * SYNCP length (just before the switch to a SYNCN)

		return start


class CPOEstimator:
	def __init__(self):
		pass

	def estimateCarrierPhaseOffsetFromPreamble(self, preamble):
		#this method is directly replicated from the one in OpenOFDM; averaging over the
		#SYNCP sections. I tried a version that compared just beginning and end copies
		#in the preamble, which performed better for the conducted and easy radiated
		#versions, but far worse in the EV charging case
		#it is probably less resistant to noise, but I suspect the main problem is missing starts of preambles causing it to get completely the wrong values
		fullamppre = preamble[HALF_SYNCP_LEN:(7 * SYNCP_LEN + HALF_SYNCP_LEN)]		#taking the seven full SYNCP blocks		#TODO: maybe shorten to avoid the SYNCP/SYNCN crossover?
		total = 0
		for i in range(0, 2688 - 384):
			sigval = fullamppre[i]
			futuresigval = fullamppre[i+384]
			total = total + (np.conj(sigval) * futuresigval)
		estim = 1/384 * np.angle(total)
		return estim

	def estimateCarrierPhaseOffsetFromCP(self, cp, remainder):
		total = 0
		cplen = len(cp)
		for i in range(cplen):
			sigval = cp[i]
			futuresigval = remainder[-cplen+i]
			total = total + (np.conj(sigval) * futuresigval)
		estim = 1 / 3072 * np.angle(total)
		return estim

class CPOCorrector:
	def __init__(self):
		pass

	def correctCarrierPhaseOffset(self, sig, cpo, start):
		#REMEMBER: the given number is what you correct by, not -ve of it (test with: Arg(Conj(complex(modulus=1, argument=1.1)) * complex(modulus=1, argument=1)) )
		#BUT: OpenOFDM (and the paper is references) are clear about computing it this way *and* using it as -j, so I guess it's right...

		#this method replaces the one below and is far faster due to numpy array operations
		j = np.complex(0, 1)
		correcs = np.full(len(sig), -j * cpo)
		indexes = np.arange(start, start+len(sig), 1)
		correcs *= indexes
		correcs = np.exp(correcs)
		sig *= correcs

		#for i in range(len(sig)):		#start from the same point as estimation did
		#	j = np.complex(0, 1)
		#	correc = np.exp(-j * (start + i) * cpo)
		#	sig[i] = sig[i] * correc
		#	##print("{}: {} vs. {}".format(start+i, correc, correcs[i]))

		return sig

class ChannelEstimator:
	def __init__(self):
		#generate the reference preamble from the template
		self.reff = np.fft.fft(hilbert(PreambleTemplate.syncp()))			#TODO: move the hilbert transform into the preamble template generation?
		self.reff = self.reff / np.real(max(np.abs(self.reff)))		#normalise the reference preamble

	def estimateChannel(self, preamble):
		channels = []
		for sym in range(0, 6):
			sec = preamble[(HALF_SYNCP_LEN + sym * SYNCP_LEN):(HALF_SYNCP_LEN + (sym+1) * SYNCP_LEN)]		#TODO: start later when SYNCP is at full power

			#fft them
			f = np.fft.fft(sec)

			#normalise the signal			#TODO: should we?
			f = f / np.real(max(np.abs(f)))

			channel = []
			for i in range(0, 192):
				if PREAMBLE_TONE_MASK[i]:
					channel.append(f[i] / self.reff[i])
				else:
					channel.append(1)	#identity for unmasked
					#channel.append(None)								#TODO: use None instead of 1 so errors happen faster

			channels.append(channel)	#no real need to keep the individual channel models, just for debugging really

		#average the channel models
		channelavg = [0] * 192
		for i in range(0, 192):
			channelavg[i] = np.mean([channels[0][i], channels[1][i], channels[2][i], channels[3][i], channels[4][i], channels[5][i]])

		return channelavg

	def generateFullChannelEstimate(self, prechannel):
		#calculate the params for interpolation (preamble scs are 8 main scs apart), but also we must filter by both
		#the preamble tone mask and also the main one so that identity or None values don't cause problems
		pcfilt = [(i*8, prechannel[i]) for i in range(0, len(prechannel)) if PREAMBLE_TONE_MASK[i] and MAIN_TONE_MASK[i*8]]
		(prescnums, prechannel) = zip(*pcfilt)

		#plotSig(np.abs([x[1] for x in pretofull]))
		#plotSig(np.angle([x[1] for x in pretofull]))

		#take a copy of the preamble channel estimate so the original is intact
		#prechannel = list(prechannel)

		#calculate the params for interpolation
		#prescnums = np.linspace(0, len(prechannel) - 1, len(prechannel))
		#prescnums *= 8

		debugPlot(prechannel, "Filtered Preamble Channel Estimate")
		#plot(prescnums, np.abs(prechannel)); plot(prescnums, np.angle(prechannel)/10); show()

		#populate full channel with preamble channel
		channelscnums = np.linspace(0, 1535, 1536)
		channel = np.interp(channelscnums, prescnums, prechannel)

		#plot(np.abs(channel)); plot(np.angle(channel)/10); show()

		#exit(1)

		return channel

class SCOEstimator:
	def __init__(self):
		pass

	def estimateSCOFromChannel(self, channel):
		#compute the slope (likely the SCO element of it)
		diffs = []
		lasti = None
		for i in range(0, 192):
			if PREAMBLE_TONE_MASK[i]:
				if lasti is None:
					lasti = i
				else:
					diffs.append(np.angle(channel[i] / channel[lasti]))
					lasti = i
		#debug("diffs: {}".format(diffs))
		#plotSig(diffs)
		#plotSig(np.angle([channel[i] for i in range(len(channel)) if PREAMBLE_TONE_MASK[i] and i >= math.floor(usableBW[0]/8) and i <= math.ceil(usableBW[1]/8)]))
		#plotSig(np.angle([channel[i] for i in range(len(channel)) if PREAMBLE_TONE_MASK[i]]))
		#debug("diffs avg: {}".format(np.mean(diffs)))

		return np.mean(diffs)

	def estimateSCOByLOBF(self, channel):
		xs = []
		ys = []
		for i in range(0, 192):
			if PREAMBLE_TONE_MASK[i]:
				xs.append(i)
				ys.append(np.angle(channel[i]))

		xm = np.mean(xs)
		ym = np.mean(ys)

		num = 0
		denom = 0
		for i in range(len(xs)):
			num += (xs[i] - xm) * (ys[i] - ym)
			denom += (xs[i] - xm)**2

		gradient = num/denom
		yintercept = ym - gradient * xm

		#plot(np.angle([channel[i] for i in range(len(channel)) if PREAMBLE_TONE_MASK[i]]))
		#show(plot(list(range(len(xs))), list(map(lambda x: yintercept + gradient * x, list(range(len(xs)))))))

		return gradient

class CyclicPrefixHandler:
	def __init__(self):
		pass

	def checkCyclicPrefix(self, sig, cplen):
		#check synchronisation
		ac = np.correlate(sig[cplen:], sig[:cplen], mode="same") / len(sig)
		expectmax = int(3072 - (cplen / 2))
		actualmax = np.argmax(ac)
		if actualmax != expectmax:
			warn("CP does not display expected autocorrelation (exp: {}, act: {})".format(expectmax, np.argmax(ac)))

		return actualmax != expectmax

	def removeCyclicPrefix(self, sig, cplen):
		#remove the cyclic prefix
		cp = sig[:cplen]
		remainder = sig[cplen:]

		#restored[3072-cplen:] = cp

		return (cp, remainder)

	def restoreCyclicPrefix(self, cp, remainder):
		remainder[3072-len(cp):] = cp
		return remainder

class Demodulator:
	SECTION_HPAVFC = 1
	SECTION_PAYLOAD = 2

	def __init__(self):
		self.phasecorrections = self.computePhaseCorrections()

	def demodulateSymbol(self, sym, rxenv, section):
		if section == self.SECTION_HPAVFC:
			cplen = HPAVRXPHY.fcguard
		elif section == self.SECTION_PAYLOAD:
			cplen = HPAVRXPHY.paysymxguard		#TODO: needs to support variable guard lengths
		else:
			raise ValueError("Unrecognised section")

		#sym = self.restoreCyclicPrefix(sym, cplen)

		active = self.countActiveSubcarriers(sym)
		if active != EXPECTED_SUBCARRIER_COUNT:
			warn("{} Active subcarriers in HPAVFC".format(active))

		f = np.fft.fft(sym)[:1536]

		#normalise the signal
		f = f / np.real(max(np.abs(f)))

		#scale the signal???
		#f = f * np.sqrt(0.5)

		#correct for channel
		f = f / rxenv.channel

		if SIG_DEBUG:
			show(plot(np.real(f), np.imag(f), "ro", label="Channel Corrected Raw Signal"))

		if SIG_DEBUG:
			from matplotlib.pyplot import scatter
			import matplotlib.cm as cm
			import matplotlib.pyplot as plt
			from matplotlib.pyplot import xlim, ylim
			scatter(np.real(f), np.imag(f), c=list(range(0, len(f))), cmap="Greys", label="Raw Signal Colour Mapped")
			xlim(-2, 2)
			ylim(-2, 2)
			plt.colorbar()
			show()

		#unmap phase mappings
		f = self.unmapPhaseOffsets(f)

		if SIG_DEBUG:
			show(plot(np.real(f), np.imag(f), "bo", label="Phase-unwrapped Signal"))

			#f2 = f[usableBW[0]:usableBW[1]]
			f2 = [f[i] for i in range(len(f)) if i >= usableBW[0] and i <= usableBW[1] and MAIN_TONE_MASK[i]]
			#plot(np.real(f), np.imag(f), "bo", label="Channel Corrected Raw Signal")
			plot(np.real(f2), np.imag(f2), "bo", label="Channel Corrected Raw Signal")
			from matplotlib.pyplot import xlim, ylim
			xlim((-2, 2))
			ylim((-2, 2))
			show()


		#f2 = f[usableBW[0]:usableBW[1]]
		#plot(np.real(f), np.imag(f), "bo", label="Channel Corrected Raw Signal")
		if False:
			f2 = [f[i] for i in range(len(f)) if i >= usableBW[0] and i <= usableBW[1] and MAIN_TONE_MASK[i]]
			plot(np.real(f2), np.imag(f2), "bo", label="Channel Corrected Raw Signal")
			plot([0, 0], [-2, 2], color="black")
			plot([-2, 2], [0, 0], color="black")
			from matplotlib.pyplot import xlim, ylim
			xlim((-2, 2))
			ylim((-2, 2))
			import time
			#filename = "test-results/{}.png".format(int((time.time() % 10000) * 100))
			filename = "test-results/{}-{}.png".format(rxenv.debugvals["runid"], int((time.time() % 10000) * 100))
			debug("Savign as: " + filename)
			from matplotlib.pyplot import savefig, close
			savefig(filename)
			close()


		#demod
		#pairs = self.softDemodulateQPSKwithBW(f, rxenv.noisevar)
		pairs = self.softDemodulateQPSKwithBWAndChannel(f, rxenv.noisevar, rxenv.channel)

		return pairs

	def restoreCyclicPrefix(self, sig, cplen):
		#check synchronisation
		ac = np.correlate(sig[cplen:], sig[:cplen], mode="same") / len(sig)
		expectmax = int(3072 - (cplen / 2))
		if np.argmax(ac) != expectmax:
			debug("WARN: CP does not display expected autocorrelation (exp: {}, act: {})".format(expectmax, np.argmax(ac)))

		#remove the cyclic prefix
		cp = sig[:cplen]
		restored = sig[cplen:]

		#f = np.fft.fft(cp)
		#plot(np.real(f), np.imag(f), "go", label="CP")
		#f = np.fft.fft(restored[-cplen:])
		#plot(np.real(f), np.imag(f), "bo", label="CP")
		#show()
		#plotTwoSigs(cp, restored[-cplen:])
		f1 = np.fft.fft(cp)
		f2 = np.fft.fft(restored[-cplen:])
		diffs = []
		for i in range(cplen):
			diff = np.angle(np.conj(f1[i]) * f2[i])
			diffs.append(diff)
		mycpoestim = np.mean(diffs) / 3072
		debug("My rolling CPO estim: {}".format(mycpoestim))

		#estimate cpo from CP
		#diffs = []
		total = 0
		for i in range(cplen):
			preval = cp[i]
			endval = restored[-cplen+i]
			total = total + (np.conj(preval) * endval)
			#diffs.append(np.angle(np.conj(cp[i]) * restored[-cplen+i]) * 1/cplen)
		#debug("Average CP diff: {}".format(np.mean(diffs)))
		#plotSig(diffs)
		#cpoestim = 1/cplen * np.angle(total)
		cpoestim = np.angle(total) / 3072
		debug("Rolling CPO estimate: {}".format(cpoestim))


		#replace the cyclic prefix in the signal							#TODO: need to correct SCO here first?
		#normalhpavfc = np.array(hpavfc)
		restored[3072-cplen:] = cp
		#plotTwoSigs(hpavfc[2500:], normalhpavfc[2500:])
		#debug("LEN cp: {}".format(len(cp)))
		#debug("LEN hpavfc: {}".format(len(hpavfc)))


		#correct cpo
		# for i in range(self.HALF_SYNCP_LEN, len(sig)):		#start from the same point as estimation did
			# j = np.complex(0, 1)
			# correc = np.exp(-j * i * cpo)
			# sig[i] = sig[i] * correc


		#TODO: reinstate this as it seemed to improve reception
		#for i in range(len(restored)):
			#j = np.complex(0, 1)
			#correc = np.exp(-j * (3072 * 6 + i) * cpoestim)
			#restored[i] = restored[i] * correc

		return restored

	def countActiveSubcarriers(self, sig):
		fp = np.abs(np.fft.fft(sig))
		thresh = np.max(fp) / 2				#np.median finds nearly the minimum value...?
		active = np.ma.count(np.where(fp > thresh))

		return active

	def computePhaseCorrections(self):
		offsets = np.concatenate((np.zeros(74), np.array(mainscphasenums) * (np.pi / 4), np.zeros(307)))
		j = np.complex(0, 1)
		correcs = np.exp(-j * offsets)
		return correcs

	def unmapPhaseOffsets(self, freqs):
		freqs = freqs * self.phasecorrections
		return freqs

	"""Soft demodulate using the method described by Rosmansyah (2003) in Soft-Demodulation of QPSK and
	16-QAM for Turbo Coded WCDMA Mobile Communication Systems  (http://epubs.surrey.ac.uk/792192/1/Rosmansyah2003.pdf)"""
	def softDemodulateQPSKwithBW(self, freqs, noisevar):
		debug("Limited BW soft demod")

		root2 = np.sqrt(2)

		dibits = []
		for i in range(0, 1536):
			if MAIN_TONE_MASK[i]:
				if i >= usableBW[0] and i <= usableBW[1]:
					y = freqs[i]

					p0 = 2/noisevar * np.real(y)/root2		#described as LOG likelihood of '0' over '1', but appears to be '1' over '0'
					p1 = 2/noisevar * np.imag(y)/root2

					dibits.append([p0, p1])
				else: dibits.append([None, None])

		return dibits

	"""Soft demodulate using the method described by Rosmansyah (2003) in Soft-Demodulation of QPSK and
	16-QAM for Turbo Coded WCDMA Mobile Communication Systems  (http://epubs.surrey.ac.uk/792192/1/Rosmansyah2003.pdf)"""
	def softDemodulateQPSKwithBWAndChannel(self, freqs, noisevar, channel):
		debug("Limited BW soft demod")

		root2 = np.sqrt(2)

		dibits_0 = 2/noisevar * np.abs(channel) * np.real(freqs) / root2		#described as LOG likelihood of '0' over '1', but appears to be '1' over '0'
		dibits_1 = 2/noisevar * np.abs(channel) * np.imag(freqs) / root2

		dibits_0 = dibits_0[MAIN_TONE_MASK][usableBW[0]:usableBW[1]]
		dibits_1 = dibits_1[MAIN_TONE_MASK][usableBW[0]:usableBW[1]]

		return list(zip(dibits_0, dibits_1))

class ChannelDeinterleaver:
	def __init__(self):
		pass

	def deinterleave(self, interbits, infobitcount, paritybitcount, stepsize, parityoffset):
		infocolsize = int(infobitcount / 4)		#always neatly divisible by four, so this is just a typecast
		paritycolsize = int(paritybitcount / 4)

		#subbanki = [None for i in range(0, infocolsize)]
		#subbankp = [None for i in range(0, paritycolsize)]

		# subbanki = [np.zeros(infocolsize), np.zeros(infocolsize), np.zeros(infocolsize), np.zeros(infocolsize)]
		# subbankp = [np.zeros(paritycolsize), np.zeros(paritycolsize), np.zeros(paritycolsize), np.zeros(paritycolsize)]
		subbanki = [np.full(infocolsize, -1), np.full(infocolsize, -2), np.full(infocolsize, -3), np.full(infocolsize, -4)]
		subbankp = [np.full(paritycolsize, -1), np.full(paritycolsize, -2), np.full(paritycolsize, -3), np.full(paritycolsize, -4)]

		#read in by row
		inforow = 0
		parityrow = 0
		for i in range(0, int(len(interbits)/4)):
			nibstart = i*4
			nibend = i*4 + 4
			nibble = interbits[nibstart:nibend]
			isparity = i % 2 != 0

			if isparity:
				rowindex = (parityrow + parityoffset) % paritycolsize
				unswitched = self.subBankBitUnswitch(nibble, i)
				subbankp[0][rowindex] = unswitched[0]
				subbankp[1][rowindex] = unswitched[1]
				subbankp[2][rowindex] = unswitched[2]
				subbankp[3][rowindex] = unswitched[3]
				parityrow += stepsize
				if parityrow >= paritycolsize:
					parityrow = (parityrow % paritycolsize) + 1
			else:
				unswitched = self.subBankBitUnswitch(nibble, i)
				subbanki[0][inforow] = unswitched[0]
				subbanki[1][inforow] = unswitched[1]
				subbanki[2][inforow] = unswitched[2]
				subbanki[3][inforow] = unswitched[3]
				inforow += stepsize
				if inforow >= infocolsize:
					inforow = (inforow % infocolsize) + 1

		#read out by column
		info = np.concatenate([subbanki[0], subbanki[1], subbanki[2], subbanki[3]])
		parity = np.concatenate([subbankp[0], subbankp[1], subbankp[2], subbankp[3]])

		#removed this once this starting propagating LLRs rather than just hard-decision bits
		#test the deinterleaving
		#if not self.testChannelDeinterleave(info, parity):
		#	#debug("WARN: Invalid values in deinterleave")
		#	raise Exception("Invalid values in channel deinterleave")

		return (info, parity)

	def subBankBitUnswitch(self, nibble, nibnum):
		switchmode = (nibnum % 8) + 1
		if switchmode == 1 or switchmode == 2: return nibble
		elif switchmode == 3 or switchmode == 4: return [nibble[3]] + nibble[:3]
		elif switchmode == 5 or switchmode == 6: return nibble[2:] + nibble[0:2]
		elif switchmode == 7 or switchmode == 8: return nibble[1:] + [nibble[0]]
		else: raise Exception("Nibble number error")

		return nibble

	def testChannelDeinterleave(self, info, parity):
		return min(info) >= 0 and min(parity) >= 0


class HPAVPostProcessor:
	def __init__(self):
		pass

	def postProcess(self, pairs):
		pass

	def reverseFCDiversityWithSoftBits(self, pairs):
		debug("Limited BW FC diversity")

		bitreplicas = [[] for x in range(0, 256)]		#start with *separate* empty lists
		for i in range(0, len(pairs)):
			iaddr = i % 256
			qaddr = (i + 128) % 256
			bitreplicas[iaddr].append(pairs[i][0])
			bitreplicas[qaddr].append(pairs[i][1])

		#take the mean of the LLRs and make a hard decision on them
		origbits = []
		for i in range(0, len(bitreplicas)):
			reps = bitreplicas[i]
			reps = list(filter(lambda x: x != None, reps))
			bit = int(np.mean(reps) > 0)
			origbits.append(bit)

		return origbits

	def reverseFCDiversityAveragingReplicaSoftBits(self, pairs):
		debug("Limited BW FC diversity")

		bitreplicas = [[] for x in range(0, 256)]		#start with *separate* empty lists
		for i in range(0, len(pairs)):
			iaddr = i % 256
			qaddr = (i + 128) % 256
			bitreplicas[iaddr].append(pairs[i][0])
			bitreplicas[qaddr].append(pairs[i][1])

		#take the mean of the LLRs *but* make no hard decision, so the turbo decoder has something to work with
		origbits = []
		for i in range(0, len(bitreplicas)):
			reps = bitreplicas[i]
			reps = list(filter(lambda x: x != None, reps))
			bit = np.mean(reps)
			origbits.append(bit)

		return origbits

class PayloadPostProcessor:
	def __init__(self):
		pass

	"""
	Based on the ROBO parameters calculation, this determines how many subcarriers need to be dropped
	to come down to the ncarrierrobo number.
	"""
	def calcSubcarriersToDrop(self, ncarrier, ncopies):
		ncarrierrobo = ncopies * math.floor(ncarrier / ncopies)
		return ncarrier - ncarrierrobo

	"""A development of the ROBO deinterleave function that combines soft bits by arithmetic mean then
	   makes a hard decision about them"""
	def roboDeinterleavewithBWAndSoftBits(self, bits, ncopies, pbsize):
		nraw = pbsize * 8 * 2		#PBsize * bitstobytes * 2 from coding
		ncarrier = 917
		bpc = 2			#QPSK

		#define the output buffer
		deinterleaved = [[] for i in range(ncopies)]
		padding = [[] for i in range(ncopies)]

		#calculate number of bits to pad
		ncarrierrobo = ncopies * math.floor(ncarrier / ncopies)
		carriersinsegment = math.floor(ncarrierrobo / ncopies)
		bitspersymbol = bpc * ncarrierrobo
		bitsinsegment = bpc * carriersinsegment
		bitsinlastsymbol = nraw - bitspersymbol * math.floor(nraw / bitspersymbol)

		if bitsinlastsymbol == 0:
			bitsinlastsymbol = bitspersymbol
			bitsinlastsegment = bitsinsegment
		else:
			bitsinlastsegment = bitsinlastsymbol - bitsinsegment * math.floor((bitsinlastsymbol - 1) / bitsinsegment)

		npad = bitsinsegment - bitsinlastsegment

		#calculate cyclic shift
		if ncopies == 2:
			#HS-ROBO
			if bitsinlastsymbol <= bitsinsegment:
				cyclicshift = [0, 0]
			else:
				cyclicshift = [0, 1]
		elif ncopies == 4:
			#ROBO
			if bitsinlastsymbol <= bitsinsegment:
				cyclicshift = [0,0,0,0]
			elif bitsinlastsymbol <= 2 * bitsinsegment:
				cyclicshift = [0,0,1,1]
			elif bitsinlastsymbol <= 3 * bitsinsegment:
				cyclicshift = [0,0,0,0]
			else:
				cyclicshift = [0,1,2,3]
		elif ncopies == 5:
			#MINI-ROBO
			if bitsinlastsymbol <= 4 * bitsinsegment:
				cyclicshift = [0,0,0,0,0]
			else:
				cyclicshift = [0,1,2,3,4]
		else:
			raise Exception("Unknown number of copies in ROBO deinterleave")

		#assign output of the robo interleaver
		for k in range(ncopies):
			if cyclicshift[k] == 0:
				#recover the bits
				for i in range(nraw):
					#debug("{}, {}, {}, {}, {}".format(k, nraw, npad, i, k * (nraw+npad) + i))
					deinterleaved[k].append(bits[k * (nraw + npad) + i])

				#recover the padding
				for i in range(npad):
					padding[k].append(bits[k * (nraw + npad) + nraw + i])
			elif cyclicshift[k] > 0:
				numberbitsshifted = (cyclicshift[k] - 1) * bitsinsegment + bitsinlastsegment

				#recover the shifted bits
				shifted = []
				for i in range(numberbitsshifted):
					#deinterleaved[k].append(bits[k * (nraw + npad) + i])
					shifted.append(bits[k * (nraw + npad) + i])

				#recover the padding
				for i in range(npad):
					padding[k].append(bits[k * (nraw + npad) + numberbitsshifted + i])

				#recover the unshifted bits
				for i in range(nraw - numberbitsshifted):
					deinterleaved[k].append(bits[k * (nraw + npad) + npad + numberbitsshifted + i])

				#put the shifted bits back on the end
				deinterleaved[k] = deinterleaved[k] + shifted
			else:
				raise Exception("Invalid cyclic shift (<0?)")

		#remove the None values and combine the various probabilities using arithmetic mean
		bitreplicas = list(zip(*deinterleaved))
		combined = []
		for i in range(0, len(bitreplicas)):
			reps = bitreplicas[i]
			reps = list(filter(lambda x: x != None, reps))
			bit = np.mean(reps) > 0
			combined.append(bit)

		return combined

	"""A development of the Mini-ROBO deinterleave function that combines soft bits by arithmetic mean and passes them on"""
	def roboDeinterleaveAveragingSoftBits(self, bits, ncopies, pbsize):
		nraw = pbsize * 8 * 2		#PBsize * bitstobytes * 2 from coding
		ncarrier = 917
		bpc = 2			#QPSK

		#define the output buffer
		deinterleaved = [[] for i in range(ncopies)]
		padding = [[] for i in range(ncopies)]

		#calculate number of bits to pad
		ncarrierrobo = ncopies * math.floor(ncarrier / ncopies)
		carriersinsegment = math.floor(ncarrierrobo / ncopies)
		bitspersymbol = bpc * ncarrierrobo
		bitsinsegment = bpc * carriersinsegment
		bitsinlastsymbol = nraw - bitspersymbol * math.floor(nraw / bitspersymbol)

		if bitsinlastsymbol == 0:
			bitsinlastsymbol = bitspersymbol
			bitsinlastsegment = bitsinsegment
		else:
			bitsinlastsegment = bitsinlastsymbol - bitsinsegment * math.floor((bitsinlastsymbol - 1) / bitsinsegment)

		npad = bitsinsegment - bitsinlastsegment

		#calculate cyclic shift
		if ncopies == 2:
			#HS-ROBO
			if bitsinlastsymbol <= bitsinsegment:
				cyclicshift = [0, 0]
			else:
				cyclicshift = [0, 1]
		elif ncopies == 4:
			#ROBO
			if bitsinlastsymbol <= bitsinsegment:
				cyclicshift = [0,0,0,0]
			elif bitsinlastsymbol <= 2 * bitsinsegment:
				cyclicshift = [0,0,1,1]
			elif bitsinlastsymbol <= 3 * bitsinsegment:
				cyclicshift = [0,0,0,0]
			else:
				cyclicshift = [0,1,2,3]
		elif ncopies == 5:
			#MINI-ROBO
			if bitsinlastsymbol <= 4 * bitsinsegment:
				cyclicshift = [0,0,0,0,0]
			else:
				cyclicshift = [0,1,2,3,4]
		else:
			raise Exception("Unknown number of copies in ROBO deinterleave")

		#assign output of the robo interleaver
		for k in range(ncopies):
			if cyclicshift[k] == 0:
				#recover the bits
				for i in range(nraw):
					deinterleaved[k].append(bits[k * (nraw + npad) + i])

				#recover the padding
				for i in range(npad):
					padding[k].append(bits[k * (nraw + npad) + nraw + i])
			elif cyclicshift[k] > 0:
				numberbitsshifted = (cyclicshift[k] - 1) * bitsinsegment + bitsinlastsegment

				#recover the shifted bits
				shifted = []
				for i in range(numberbitsshifted):
					#deinterleaved[k].append(bits[k * (nraw + npad) + i])
					shifted.append(bits[k * (nraw + npad) + i])

				#recover the padding
				for i in range(npad):
					padding[k].append(bits[k * (nraw + npad) + numberbitsshifted + i])

				#recover the unshifted bits
				for i in range(nraw - numberbitsshifted):
					deinterleaved[k].append(bits[k * (nraw + npad) + npad + numberbitsshifted + i])

				#put the shifted bits back on the end
				deinterleaved[k] = deinterleaved[k] + shifted
			else:
				raise Exception("Invalid cyclic shift (<0?)")

		#remove the None values and combine the various probabilities using arithmetic mean
		bitreplicas = list(zip(*deinterleaved))
		combined = []
		for i in range(0, len(bitreplicas)):
			reps = bitreplicas[i]
			reps = list(filter(lambda x: x != None, reps))
			bit = np.mean(reps)
			combined.append(bit)

		return combined

	"""
	unscramble <- function(bits) {
		x10 = c(1,1,1,1,1,1,1)
		x3 = c(1,1,1)
		#x0 = c(1)

		outbits = vector(length=length(bits))
		for (i in seq(1, length(bits))) {
			bit = bits[i]
			x10_3 = xor(x10[1], x3[1])
			#x3_0???????
			x10 = shiftin(x10, x3[1])
			newbit = xor(bit, x10_3)
			#x3 = shiftin(x3, bit)
			x3 = shiftin(x3, x10_3)
			#outbits[i] = xor(bit, x10_3)
			outbits[i] = newbit
			##x3 = shiftin(x3, x0)
			##x0 = shiftin(x0, x10_3)
			#outbits[i] = xor(bit, x10_3)
		}
		return(outbits)
	}
	"""
	def unscramble(self, bits):
		x10 = [1,1,1,1,1,1,1]
		x3 = [1,1,1]

		outbits = []

		for i in range(len(bits)):
			bit = bits[i]

			x10_3 = x10[0] ^ x3[0]
			x10 = self.shiftin(x10, x3[0])
			newbit = bit ^ x10_3
			x3 = self.shiftin(x3, x10_3)
			outbits.append(newbit)

		return outbits

	def shiftin(self, existing, new):
		existing.pop(0)
		return existing + [new]

class FECDecoder:
	def __init__(self):
		self.turbointer = TurboInterleaver()

	def decode(self, infobits, paritybits, infobits2, paritybits2):
		infoprobs = [(self.initlog(-x), self.initlog(x)) for x in infobits2]	#as the LLR seems to be reversed it is this way round
		parityprobs = [(self.initlog(-x), self.initlog(x)) for x in paritybits2]	#as the LLR seems to be reversed it is this way round

		#print("HERE1: {}".format(infobits[0:10]))
		#print("HERE2: {}".format(infobits2[0:10]))
		#print("HERE3: {}".format(infoprobs[0:10]))

		if len(infobits) != len(paritybits):
			raise ValueError("Info and parity different lengths")

		cwprobs = [(infoprobs[2*i], infoprobs[2*i+1], parityprobs[2*i]) for i in range(int(len(infoprobs)/2))]
		#cwprobs = [((0.5, 0.5), (0.5, 0.5), (0.5, 0.5)) for i in range(int(len(infoprobs)/2))]
		fudgeprobs = lambda x: 0.9 if x == 1.0 else 0.1 if x == 0 else x
		cwprobs = [((fudgeprobs(p[0][0]), fudgeprobs(p[0][1])), (fudgeprobs(p[1][0]), fudgeprobs(p[1][1])), (fudgeprobs(p[2][0]), fudgeprobs(p[2][1]))) for p in cwprobs]
		#print("HERE4-1: {}".format(cwprobs))

		code = HomePlugAVConvolutionalCode()
		trellis = Trellis(code, int(len(infobits)/code.ninput) + 1, 3, False)
		decoder = BCJRDecoder()

		apriori = [((0.5, 0.5), (0.5, 0.5))] * len(infobits)

		(bits, pplus, pminus) = decoder.decode(apriori, cwprobs, trellis)

		#print("HERE5: {}".format(bits))
		#print("HERE6: {}".format(infobits))

		return np.array(bits)

	def turboDecode(self, infobits, paritybits, pbsize, turboiterations):

		#get the bit probability pairs together
		infoprobs = [(self.initlog(-x), self.initlog(x)) for x in infobits]	#as the LLR seems to be reversed it is this way round
		parityprobs = [(self.initlog(-x), self.initlog(x)) for x in paritybits]	#as the LLR seems to be reversed it is this way round

		#values for interleaving
		pbsizebits = pbsize * 8
		mapping = self.turbointer.params[pbsize][4]

		#interleave the bit probs
		interinfoprobs = self.turbointer.interleave(pbsizebits, mapping, infoprobs)

		#pack the bit probs into threes per codeword, normal and interleaved
		cwprobs = [(infoprobs[2*i], infoprobs[2*i+1], parityprobs[2*i]) for i in range(int(len(infoprobs)/2))]
		intercwprobs = [(interinfoprobs[2*i], interinfoprobs[2*i+1], parityprobs[2*i+1]) for i in range(int(len(infoprobs)/2))]

		#hack around floating point limitations
		fudgeprobs = lambda x: 0.9999999999999 if x > 0.9999999999999 else 0.0000000000001 if x < 0.0000000000001 else x
		cwprobs = [((fudgeprobs(p[0][0]), fudgeprobs(p[0][1])), (fudgeprobs(p[1][0]), fudgeprobs(p[1][1])), (fudgeprobs(p[2][0]), fudgeprobs(p[2][1]))) for p in cwprobs]
		intercwprobs = [((fudgeprobs(p[0][0]), fudgeprobs(p[0][1])), (fudgeprobs(p[1][0]), fudgeprobs(p[1][1])), (fudgeprobs(p[2][0]), fudgeprobs(p[2][1]))) for p in intercwprobs]

		#create the code and decoder objects
		code = HomePlugAVConvolutionalCode()
		trellis = Trellis(code, int(len(infobits)/code.ninput) + 1, 3, False)
		decoder = BCJRDecoder()

		#arrange the initial values
		#apriori = [((0.5, 0.5), (0.5, 0.5))] * len(infobits)
		apriori = [((0.5, 0.5), (0.5, 0.5))] * int(len(infobits)/2)						#TODO: this doesn't cause an error, so something is wrong with above
		extrinsics = None
		interextrinsics = None

		repackextrinsic = lambda x: [(x[2*i], x[2*i+1]) for i in range(int(len(x)/2))]
		unpackextrinsic = lambda x: [z for y in x for z in y]
		#extrinsictoapriori = lambda x: [(z, (0.5, 0.5)) for y in x for z in y]
		extrinsictoapriori = lambda x: x

		for i in range(turboiterations):
			print("Turbo Iteration: {}/{}".format(i, turboiterations))

			#do the normal decoding
			#print("Apriori (len: {}): {}".format(len(apriori), apriori[:10]))
			#print("Cwprobs (len: {}): {}".format(len(cwprobs), cwprobs[:10]))
			(probs, pplus, pminus) = decoder.decode(apriori, cwprobs, trellis, softoutput=True)
			#return np.array([int(x[0] > x[1]) for x in probs])
			probs = [(fudgeprobs(p[0]),fudgeprobs(p[1])) for p in probs]							#maybe need to normalise too?
			#print("Probs (len: {}): {}".format(len(repackextrinsic(probs)), repackextrinsic(probs)[:10]))

			#calculate the extrinsic values
			extrinsics = self.computeExtrinsics(repackextrinsic(probs), apriori, cwprobs)
			#print("Extrinsics (len: {}): {}".format(len(extrinsics), extrinsics[:10]))
			extrinsics = [((fudgeprobs(e[0][0]),fudgeprobs(e[0][1])), (fudgeprobs(e[1][0]),fudgeprobs(e[1][1]))) for e in extrinsics]

			#prepare resulting values for the interleaved decoding
			#print("Extrinsics2 (len: {}): {}".format(len(extrinsics), extrinsics[:10]))
			#interapriori = self.turbointer.interleave(pbsizebits, mapping, extrinsictoapriori(extrinsics))
			interapriori = repackextrinsic(self.turbointer.interleave(pbsizebits, mapping, unpackextrinsic(extrinsics)))
			#print("Interapriori (len: {}): {}".format(len(interapriori), interapriori[:10]))

			(interprobs, interpplus, interpminus) = decoder.decode(interapriori, intercwprobs, trellis, softoutput=True)
			#return np.array(self.turbointer.deinterleave(pbsizebits, mapping, [int(x[0] > x[1]) for x in interprobs]))
			interprobs = [(fudgeprobs(p[0]),fudgeprobs(p[1])) for p in interprobs]
			interextrinsics = self.computeExtrinsics(repackextrinsic(interprobs), interapriori, intercwprobs)
			interextrinsics = [((fudgeprobs(e[0][0]),fudgeprobs(e[0][1])), (fudgeprobs(e[1][0]),fudgeprobs(e[1][1]))) for e in interextrinsics]
			#print("Interextrinsics (len: {}): {}".format(len(interextrinsics), interextrinsics[:10]))

			#apriori = self.turbointer.deinterleave(pbsizebits, mapping, extrinsictoapriori(interextrinsics))
			apriori = repackextrinsic(self.turbointer.deinterleave(pbsizebits, mapping, unpackextrinsic(interextrinsics)))


		finalprobs = self.turbointer.deinterleave(pbsizebits, mapping, interprobs)
		#print("FINALPROBS" + str(finalprobs))
		bits = [int(x[0] > x[1]) for x in finalprobs]

		#print("HERE5: {}".format(bits))
		#print("HERE6: {}".format(infobits))

		return np.array(bits)


	def computeExtrinsics(self, likelihoods, aprioris, recvs):
		extrinsics = []
		for i in range(len(likelihoods)):
			(l1, l2) = likelihoods[i]
			(a1, a2) = aprioris[i]
			(c1, c2, _) = recvs[i]

			a1 = (1e-30 if a1[0] == 0 else a1[0], 1e-30 if a1[1] == 0 else a1[1])	#hax
			a2 = (1e-30 if a2[0] == 0 else a2[0], 1e-30 if a2[1] == 0 else a2[1])	#hax
			c1 = (1e-30 if c1[0] == 0 else c1[0], 1e-30 if c1[1] == 0 else c1[1])	#hax
			c2 = (1e-30 if c2[0] == 0 else c2[0], 1e-30 if c2[1] == 0 else c2[1])	#hax


			e1 = ((l1[0] / a1[0]) / c1[0], (l1[1] / a1[1]) / c1[1])
			#e2 = ((l1[0] / a1[0]) / c1[0], (l1[1] / a1[1]) / c1[1])			#TODO: shouldn't this be everything2 instead?
			e2 = ((l2[0] / a2[0]) / c2[0], (l2[1] / a2[1]) / c2[1])			#TODO: like this?
			norm1 = e1[0] + e1[1]
			norm2 = e2[0] + e2[1]
			#(e1, e2) = ((e1[0] / norm1, e1[1] / norm1), (e2[0] / norm2, e2[1] / norm2))
			(e1, e2) = ((1-(e1[0] / norm1), 1-(e1[1] / norm1)), (1-(e2[0] / norm2), 1-(e2[1] / norm2)))		#lol, not doing this of course ruins the results because the probabilities are the wrong way round...
			extrinsics.append((e1, e2))

		return extrinsics

	def initlog(self, v):
		try:
			#return math.log( math.exp(v)/(1 + math.exp(v)) )
			return math.exp(v)/(1 + math.exp(v))
		except OverflowError as e:
			return 1.0
			#return 1 - (math.exp(-v)/(1 + math.exp(-v)))
		except Exception as e:
			print(v)
			print(e)
			raise e

	def compareDecode(self, bits, infobits):
		codediffs = 0
		for i in range(len(bits)):
			if bits[i] != infobits[i]:
				codediffs += 1
				debug("Diff at position {}: {} vs. {}".format(i, bits[i], infobits[i]))
		debug("HERE7: {}".format(codediffs))
		debug("HERE8: {} and {}".format(type(bits), type(infobits)))
