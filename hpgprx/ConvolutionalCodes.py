import math
import random
import numpy as np

class Trellis:
	def __init__(self, code, length, statebits, terminated):
		if terminated:
			raise Exception("Support for terminated trellises removed after commit 3de46fc42a0688fa5cea08ca2e20bee83d675de7")
		
		self.code = code
		self.transitions = {}
		self.transitionvaliditymatrix = None
		self.transitioninputs = []
		self.transitioncodewords = []
		self.length = length
		self.statebits = statebits
		self.terminated = terminated
		
		self.deriveTransitions(code)
	
	"""Nicer-sounding interface function"""
	def setCode(self, code):
		#self.deriveTransitions(code)
		raise UnimplementedError()
	
	def stateToNum(self, s):
		return s[0]*4+s[1]*2+s[2]
	
	def deriveTransitions(self, code):	
		#for each state
		startstates = self.generateStates()
		
		#define a transition validity matrix for quick operations
		self.transitionvaliditymatrix = np.zeros(shape=(len(startstates),len(startstates)))
		self.transitioninputs = [np.zeros(shape=(len(startstates),len(startstates))) for i in range(code.ninput)]
		self.transitioncodewords = [np.zeros(shape=(len(startstates),len(startstates))) for i in range(code.codewordsize)]
		
		#derive the transitions for the general (fully-connected state)
		for startstate in startstates:
			self.transitions[startstate] = {}
			
			#for input in [0, 1]:
			for input in self.generateInputs(self.code.ninput):
				code.state = list(startstate)
				cw = code.advance(input)
				endstate = tuple(code.state)
				self.transitions[startstate][endstate] = (input, cw)
				
				self.transitionvaliditymatrix[self.stateToNum(startstate), self.stateToNum(endstate)] = 1
				for i in range(len(input)):
					self.transitioninputs[i][self.stateToNum(startstate), self.stateToNum(endstate)] = input[i]
				for i in range(len(cw)):
					self.transitioncodewords[i][self.stateToNum(startstate), self.stateToNum(endstate)] = cw[i]
			
				#print("{} -- {}/{} --> {}".format(startstate, input, cw, endstate))
		
	
	def generateStates(self):
		bitfield = lambda n: [1 if digit=='1' else 0 for digit in bin(n)[2:]]
		pad = lambda b: [0] * (self.statebits - len(b)) + b
		return [tuple(pad(bitfield(i))) for i in range(2**self.statebits)]

	def generateStateNums(self):
		return list(range(2**self.statebits))
	
	def generateInputs(self, ninputs):
		bitfield = lambda n: [1 if digit=='1' else 0 for digit in bin(n)[2:]]
		pad = lambda b: [0] * (ninputs - len(b)) + b
		return [tuple(pad(bitfield(i))) for i in range(2**ninputs)]
	
	def zeroState(self):
		return (0,) * self.statebits
	
	def validEdge(self, start, end, t):
		return end in self.transitions[start]
	
	def inputBitToTransition(self, start, end, i, t):
		#if not self.validEdge(start, end, t):													#it would be wise to have this, but it gets hit so many times the performance improves without it
		#	raise ValueError("Not a valid transition")
		#print("Trans: {} -> {} : {}".format(start, end, self.transitions[start][end]))
		return self.transitions[start][end][0][i]
	
	def codewordBitToTransition(self, start, end, i, t):
		#if not self.validEdge(start, end, t):													#it would be wise to have this, but it gets hit so many times the performance improves without it
		#	raise ValueError("Not a valid transition")
		return self.transitions[start][end][1][i]
	
	def inSPlus(self, start, end, i, t):
		#if not self.validEdge(start, end, t):													#it would be wise to have this, but it gets hit so many times the performance improves without it
		#	raise ValueError("Not a valid transition")
		return self.transitions[start][end][0][i] == 1
	
	def inSMinus(self, start, end, i, t):
		#if not self.validEdge(start, end, t):													#it would be wise to have this, but it gets hit so many times the performance improves without it
		#	raise ValueError("Not a valid transition")
		return self.transitions[start][end][0][i] == 0

class ConvolutionalCode:
	def __init__(self):
		pass
	
	def xor(self, a, b):
		return int(a != b)
	
	def headOfState(self):
		return self.state[-1]
	
	def advanceState(self, bit):
		self.state = [bit] + self.state[:-1]

class HomePlugAVConvolutionalCode(ConvolutionalCode):
	def __init__(self):
		self.statedepth = 3
		self.state = [0, 0, 0]
		self.ninput = 2
		self.codewordsize = 3
	
	def advance(self, bittuple):
		(bit1, bit2) = bittuple
		codeword = [bit1, bit2, None]
		x0 = self.xor(bit1, self.xor(bit2, self.headOfState()))
		codeword[2] = x0
		s3 = self.xor(x0, self.xor(bit2, self.state[1]))
		s2 = self.xor(bit1, self.xor(bit2, self.state[0]))
		s1 = self.xor(x0, self.xor(bit1, bit2))
		self.state = [s1, s2, s3]
		return codeword	

class BCJRDecoder():
	def __init__(self):
		pass
	
	def decode(self, apriori, recv, trellis, chanprobs=None, softoutput=False):
		states = trellis.generateStates()
		
		#initialise the gamma values
		gamma = [np.copy(trellis.transitionvaliditymatrix) for i in range(trellis.length)]
		for t in range(1, trellis.length):
			for ui in range(trellis.code.ninput):
				bitones = trellis.transitioninputs[ui]
				bitzeros = 1.0 - bitones
				
				gamma[t][np.nonzero(bitones)] *= apriori[t-1][ui][1]
				gamma[t][np.nonzero(bitzeros)] *= apriori[t-1][ui][0]
		
			for ci in range(trellis.code.codewordsize):
				bitones = trellis.transitioncodewords[ci]
				bitzeros = 1.0 - bitones
				
				gamma[t][np.nonzero(bitones)] *= recv[t-1][ci][1]
				gamma[t][np.nonzero(bitzeros)] *= recv[t-1][ci][0]
		

		#init alpha before forward propagation
		alpha = [np.zeros(shape=(len(states),1)) for i in range(trellis.length)]
		for i in range(len(states)):
			alpha[0][i] = 0.5**trellis.statebits

		#forward propagation
		for t in range(1, trellis.length):
			alpha[t] = np.matmul(np.transpose(gamma[t]), alpha[t-1])
			alpha[t] /= np.sum(alpha[t])
		
		#init beta before backward propagation
		beta = [np.zeros(shape=(len(states),1)) for i in range(trellis.length)]
		for i in range(len(states)):
			beta[trellis.length-1][i] = 0.5**trellis.statebits
			beta[0][i] = 0.5**trellis.statebits
		
		#backward propagation
		for t in range(trellis.length -2, 0, -1):
			beta[t] = np.matmul(gamma[t+1], beta[t+1])
			beta[t] /= np.sum(beta[t])
		
		#init m values
		m = [np.zeros(shape=(len(states),len(states))) for i in range(trellis.length)]
		
		#m values
		for t in range(1, trellis.length):
			m[t] = alpha[t-1] * gamma[t] * np.transpose(beta[t])
			m[t] /= np.sum(m[t])		
		
		#init bit probabilities
		pplus = [[0.0 for j in range(trellis.code.ninput)] for i in range(trellis.length)]
		pminus = [[0.0 for j in range(trellis.code.ninput)] for i in range(trellis.length)]
		
		#bit probabilities
		for t in range(1, trellis.length):
			for ui in range(trellis.code.ninput):
				bitones = trellis.transitioninputs[ui]
				bitzeros = 1.0 - bitones
				
				pplus[t][ui] += np.sum(m[t][np.nonzero(bitones)])
				pminus[t][ui] += np.sum(m[t][np.nonzero(bitzeros)])		
		
		#prepare output
		if softoutput:
			bits = [(pplus[t][i], pminus[t][i]) for t in range(1, trellis.length) for i in range(trellis.code.ninput)]			#output soft decisions
		else:
			bits = [int(pplus[t][i] > pminus[t][i]) for t in range(1, trellis.length) for i in range(trellis.code.ninput)]			#output hard decisions

		return (bits, pplus, pminus)


class AWGNChannel:
	def __init__(self, noisesd):
		self.noisesd = noisesd
	
	def send(self, codewords):
		#return [(a + random.gauss(0, self.noisesd), b + random.gauss(0, self.noisesd)) for (a,b) in codewords]
		return self.addNoise([self.modulate(tuple(x)) for x in codewords], self.noisesd)
	
	def calcProbability(self, recvbit):
		dist0 = abs(0 - recvbit)
		dist1 = abs(1 - recvbit)
		tot = dist0 + dist1
		return (dist1 / tot, dist0 / tot)		#distances swap round as smaller dist = greater probability
		
	def initlog(self, v):
		try:
			#return math.log( math.exp(v)/(1 + math.exp(v)) )
			return math.exp(v)/(1 + math.exp(v))
		except OverflowError as e:
			return 1.0
			#return 1 - (math.exp(-v)/(1 + math.exp(-v)))
		except Exception as e:
			print(v)
			print(e)
			raise e
		
	def calcProbabilities(self, received):
		#return [(self.calcProbability(x1), self.calcProbability(x2)) for (x1, x2) in received]
		initlog = lambda v: math.log( math.exp(v)/(1 + math.exp(v)) )
		#return [((initlog(x1), initlog(-x1)), (initlog(x2), initlog(-x2))) for (x1, x2) in self.logLikelihoodAWGNSoftDemod(received, self.noisesd)]
		return [((self.initlog(-x1), self.initlog(x1)), (self.initlog(-x2), self.initlog(x2))) for (x1, x2) in self.logLikelihoodAWGNSoftDemod(received, self.noisesd)]
	
	def logLikelihoodAWGNSoftDemod(self, vals, noisesd):
		import numpy as np
		noisevar = noisesd ** 2
		noisevar = 1e-100 if noisevar == 0 else noisevar#small hack
		
		root2 = np.sqrt(2)

		bitvals = []
		for i in range(len(vals)):
			y = vals[i]

			p0 = 2/noisevar * np.real(y)/root2		#described as LOG likelihood of '0' over '1', but appears to be '1' over '0'
			p1 = 2/noisevar * np.imag(y)/root2
			bitvals.append((p0, p1))
		
		return bitvals
	
	def modulate(self, pair):
		import numpy as np
	
		sqrthalf = np.sqrt(0.5)
		if pair == (0, 0):
			return np.complex(-sqrthalf, -sqrthalf)
		elif pair == (0, 1):
			return np.complex(-sqrthalf, sqrthalf)
		elif pair == (1, 0):
			return np.complex(sqrthalf, -sqrthalf)
		elif pair == (1, 1):
			return np.complex(sqrthalf, sqrthalf)
		else:
			raise Exception(str(pair))

	def addNoise(self, vals, noisesd):
		import numpy as np

		disti = np.random.normal(loc=0, scale=noisesd, size=len(vals))		#this will overestimate as it uses two univariate distribs instead of one complex one
		distq = np.random.normal(loc=0, scale=noisesd, size=len(vals)) * 1j

		return vals + disti + distq



def testHPAVCode(ninbits, crossoverprob):
	code = HomePlugAVConvolutionalCode()

	if ninbits % code.ninput != 0:
		raise ValueError("Number of input bits is not a multiple of the code's input")
	
	inbits = []
	codewords = []
	last = None
	for i in range(ninbits):
		bit = int(random.random() >= 0.5)
		inbits.append(bit)
		if last is not None:
			codeword = code.advance((last, bit))
			codewords.append(codeword)
			last = None
		else:
			last = bit

	for i in range(3):
		if code.state == [0, 0, 0]:
			pad = (0, 0)
		elif code.state == [0, 0, 1]:
			pad = (1, 1)
		elif code.state == [0, 1, 0]:
			pad = (1, 1)
		elif code.state == [0, 1, 1]:
			pad = (0, 0)
		elif code.state == [1, 0, 0]:
			pad = (0, 1)
		elif code.state == [1, 0, 1]:
			pad = (1, 1)
		elif code.state == [1, 1, 0]:
			pad = (1, 0)
		elif code.state == [1, 1, 1]:
			pad = (0, 0)
		else:
			raise ValueError()
		inbits.append(pad[0])
		inbits.append(pad[1])
		codewords.append(code.advance(pad))
	
	#print(len(inbits))
	#print(inbits)
	#print(len(codewords))
	#print(codewords)
	
	#unpackcw = [x for y in codewords for x in y]		#unpack the triplets and repack into pairs for QPSK
	unpackcw = unpack(codewords)
	#print(len(unpackcw))
	#print(unpackcw)
	#qpskcw = [[unpackcw.pop(0), unpackcw.pop(0)] for i in range(math.floor(len(unpackcw)/2))]
	qpskcw = repack(unpackcw, 2)
	#print(len(qpskcw))
	#print(qpskcw)
		
	trellis = Trellis(code, int(ninbits/code.ninput) + 3 + 1, 3, True)	
	#trellis.setCode(code)

	decoder = BCJRDecoder()
	
	apriori = [((0.5, 0.5), (0.5, 0.5))] * ninbits + [((0.5, 0.5), (0.5, 0.5))] * 3		#source bits + padding bits may (but in this case don't) have different priors
	
	channel = AWGNChannel(crossoverprob)
	received = channel.send(qpskcw)
	
	#print(len(received))
	#print(received)

	qpskrecvprobs = channel.calcProbabilities(received)
	
	unpackrp = unpack(qpskrecvprobs)
	recvprobs = repack(unpackrp, 3)
	
	#print(len(recvprobs))
	#print(recvprobs)
	
	#(bits, pplus, pminus) = decoder.decode(apriori, received, trellis, chanprobs)
	(bits, pplus, pminus) = decoder.decode(apriori, recvprobs, trellis)
	
	#calculate the metrics for the uncoded equivalent 
	corbits = [x + random.gauss(0, crossoverprob) for x in inbits[:ninbits]]
	plainrecv = [int(x>0.5) for x in corbits]
	plainbiterrors = sum([1 if a != b else 0 for (a,b) in zip(inbits, plainrecv)])
	
	#print(inbits)
	#print(bits)
	
	biterrors = sum([1 if a != b else 0 for (a,b) in zip(inbits, bits)])
	
	#print(biterrors)
	
	return((plainbiterrors, biterrors, 0))


def unpack(input):
	return [x for y in input for x in y]

def repack(input, num):
	if len(input) % num != 0:
		raise ValueError("Cannot repack as input is not divisible by {}".format(num))
	packed = []
	for i in range(math.floor(len(input)/num)):
		packed.append(tuple([input.pop(0) for x in range(num)]))
	return packed
