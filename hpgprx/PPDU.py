import math
import numpy as np
from matplotlib.pyplot import plot, show
from scipy.signal import hilbert
import struct

from .utils import *
from .HPAVEnv import *

"""
crc32alt <- function(bits) {
	#as numerical design was failing, this circuit implementation was derived from Mathworks CRC-N Generator (https://uk.mathworks.com/help/comm/ref/crcngenerator.html)
	#and the HomePlug 1.0 specification, which has an example of the circuit for a CRC-16 meeting our needs on page 66

	x26 = c("1","1","1","1","1","1")
	x23 = c("1","1","1")
	x22 = c("1")
	x16 = c("1","1","1","1","1","1")
	x12 = c("1","1","1","1")
	x11 = c("1")
	x10 = c("1")
	x8 = c("1","1")
	x7 = c("1")
	x5 = c("1","1")
	x4 = c("1")
	x2 = c("1","1")
	x1 = c("1")
	x0 = c("1")

	for (i in seq(1, length(bits))) {
		bit = bits[i]
		bit = xor(bit, x26[1])
		x26 = shiftin(x26, xor(bit, x23[1]))
		x23 = shiftin(x23, xor(bit, x22[1]))
		x22 = shiftin(x22, xor(bit, x16[1]))
		x16 = shiftin(x16, xor(bit, x12[1]))
		x12 = shiftin(x12, xor(bit, x11[1]))
		x11 = shiftin(x11, xor(bit, x10[1]))
		x10 = shiftin(x10, xor(bit, x8[1]))
		x8 = shiftin(x8, xor(bit, x7[1]))
		x7 = shiftin(x7, xor(bit, x5[1]))
		x5 = shiftin(x5, xor(bit, x4[1]))
		x4 = shiftin(x4, xor(bit, x2[1]))
		x2 = shiftin(x2, xor(bit, x1[1]))
		x1 = shiftin(x1, xor(bit, x0[1]))
		x0 = shiftin(x0, bit)

		remainder = c(x26, x23, x22, x16, x12, x11, x10, x8, x7, x5, x4, x2, x1, x0)
		#print(paste(remainder, collapse=""))
	}

	return(onescomplement(remainder))
}
"""
class CRC32ALT:
	def calc(self, bits):
		x26 = [1,1,1,1,1,1]
		x23 = [1,1,1]
		x22 = [1]
		x16 = [1,1,1,1,1,1]
		x12 = [1,1,1,1]
		x11 = [1]
		x10 = [1]
		x8 = [1,1]
		x7 = [1]
		x5 = [1,1]
		x4 = [1]
		x2 = [1,1]
		x1 = [1]
		x0 = [1]
		
		remainder = x26 + x23 + x22 + x16 + x12 + x11 + x10 + x8 + x7 + x5 + x4 + x2 + x1 + x0
		
		for i in range(0, len(bits)):
			bit = bits[i]
			bit = bit ^ x26[0]
			x26 = self.shiftin(x26, bit ^ x23[0])
			x23 = self.shiftin(x23, bit ^ x22[0])
			x22 = self.shiftin(x22, bit ^ x16[0])
			x16 = self.shiftin(x16, bit ^ x12[0])
			x12 = self.shiftin(x12, bit ^ x11[0])
			x11 = self.shiftin(x11, bit ^ x10[0])
			x10 = self.shiftin(x10, bit ^ x8[0])
			x8 = self.shiftin(x8, bit ^ x7[0])
			x7 = self.shiftin(x7, bit ^ x5[0])
			x5 = self.shiftin(x5, bit ^ x4[0])
			x4 = self.shiftin(x4, bit ^ x2[0])
			x2 = self.shiftin(x2, bit ^ x1[0])
			x1 = self.shiftin(x1, bit ^ x0[0])
			x0 = self.shiftin(x0, bit)
		
		return self.onescomplement(x26 + x23 + x22 + x16 + x12 + x11 + x10 + x8 + x7 + x5 + x4 + x2 + x1 + x0)
			
	def shiftin(self, existing, new):
		existing.pop(0)
		return existing + [new]

	def onescomplement(self, bits):
		res = [1 if b == 0 else 0 for b in bits]
		return res

class CRC24ALT:
	def calc(self, bits):
		x23 = [1]
		x6 = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
		x5 = [1]
		x1 = [1,1,1,1]
		x0 = [1]
		
		remainder = x23 + x6 + x5 + x1 + x0
		
		for i in range(0, len(bits)):
			bit = bits[i]
			bit = bit ^ x23[0]
			x23 = self.shiftin(x23, bit ^ x6[0])
			x6 = self.shiftin(x6, bit ^ x5[0])
			x5 = self.shiftin(x5, bit ^ x1[0])
			x1 = self.shiftin(x1, bit ^ x0[0])
			x0 = self.shiftin(x0, bit)
		
		return self.onescomplement(x23 + x6 + x5 + x1 + x0)
			
	def shiftin(self, existing, new):
		existing.pop(0)
		return existing + [new]

	def onescomplement(self, bits):
		res = [1 if b == 0 else 0 for b in bits]
		return res


class AVFrameControlVariant:
	def __init__(self, fctype, bits):
		self.rawbits = bits
		
		self.fields = {}
		ext = BitsExtractor(bits)
	
		if fctype == AVFrameControl.DT_BEACON:
			self.fields["bts"] = int.from_bytes(bitsToBytes(ext.get(32)), byteorder='little', signed=False)
			self.fields["bto1"] = int.from_bytes(bitsToBytes(ext.get(16)), byteorder='little', signed=False)
			self.fields["bto2"] = int.from_bytes(bitsToBytes(ext.get(16)), byteorder='little', signed=False)
			self.fields["bto3"] = int.from_bytes(bitsToBytes(ext.get(16)), byteorder='little', signed=False)
			self.fields["bto4"] = int.from_bytes(bitsToBytes(ext.get(16)), byteorder='little', signed=False)
		elif fctype == AVFrameControl.DT_SOF:
			self.fields["stei"] = bitsToBytes(ext.get(8))
			self.fields["dtei"] = bitsToBytes(ext.get(8))
			self.fields["lid"] = bitsToBytes(ext.get(8))
			self.fields["cfs"] = bitsToBytes(ext.get(1))
			self.fields["bdf"] = bitsToBytes(ext.get(1))
			self.fields["bp10df"] = bitsToBytes(ext.get(1))
			self.fields["hp11df"] = bitsToBytes(ext.get(1))
			self.fields["eks"] = bitsToBytes(ext.get(4))
			self.fields["ppb"] = bitsToBytes(ext.get(8))
			self.fields["ble"] = bitsToBytes(ext.get(8))
			self.fields["pbsz"] = bitsToBytes(ext.get(1))
			self.fields["numsym"] = bitsToBytes(ext.get(2))
			self.fields["tmi_av"] = bitsToBytes(ext.get(5))
			self.fields["fl_av"] = int.from_bytes(bitsToBytes(ext.get(12)), byteorder='little', signed=False)
			self.fields["mpducnt"] = bitsToBytes(ext.get(2))
			self.fields["burstcnt"] = bitsToBytes(ext.get(2))
			self.fields["bbf"] = bitsToBytes(ext.get(1))
			self.fields["mrtfl"] = bitsToBytes(ext.get(4))
			self.fields["dcppcf"] = bitsToBytes(ext.get(1))
			self.fields["mcf"] = bitsToBytes(ext.get(1))
			self.fields["mnbf"] = bitsToBytes(ext.get(1))
			self.fields["rsr"] = bitsToBytes(ext.get(1))
			self.fields["clst"] = bitsToBytes(ext.get(1))
		else:
			debug("Variant for fctype {} not implemented".format(fctype))
			
		if not ext.checkDone():
			debug("WARN: Not all bits extracted in AVFC Variant")

class AVFrameControl:
	DATA_LENGTH = 104
	
	DT_BEACON = 0
	DT_SOF = 1
	DT_SACK = 2
	DT_RTS_CTS = 3
	DT_SOUND = 4
	DT_REVERSE_SOF = 5

	def __init__(self, bits):
		self.rawbits = bits
		ext = BitsExtractor(bits)
		
		self.delimtype = int.from_bytes(bitsToBytes(ext.get(3)), byteorder='little', signed=False)
		self.access = int.from_bytes(bitsToBytes(ext.get(1)), byteorder='little', signed=False)
		self.snid = bitsToBytes(ext.get(4))
		self.variant = AVFrameControlVariant(self.delimtype, ext.get(96))
		self.checksum = ext.get(24)		#this stays as bits for the calculation
		
		if not ext.checkDone():
			debug("WARN: Not all bits extracted in AVFC")
		
		self.checksumcalc = CRC24ALT().calc(bits[0:self.DATA_LENGTH])
	
	def isChecksumOk(self):
		debug("Calculated FC CRC24: {}".format(self.checksumcalc))
		debug("Transmitted FC CRC24: {}".format(list(self.checksum)))

		return self.checksumcalc == list(self.checksum)
	
	"""Returns the number of symbols for this PPDU. This depends on the PPDU type and sometimes 
	   also on values in the Frame Control. For instance, a beacon is of fixed bit length and 
	   always uses MINI-ROBO, so the number of symbols depends only on the Tone Mask (with 
	   typically 6 symbols used for the standard 917-subcarrier mask). A message frame indicates 
	   the frame length in its header. 
	"""
	def getSymbolCount(self):
		if self.delimtype == self.DT_BEACON:
			return 6		#TODO: should really depend on active subcarriers
		elif self.delimtype == self.DT_SOF:
			#the formula actually turned out to be quite simple
			flav = self.variant.fields["fl_av"]
			rifs = 10500 / 75		#default RIFS_AV = 140usecs
			tmi = int.from_bytes(self.variant.fields["tmi_av"], byteorder="little", signed=False)
			gi = 567 if tmi == 2 else 417							#in HPGP they are not link-dependent, just based on which ROBO mode
			symbol = (3072 + gi) / 75
			
			syms = int(round(((flav * 1.28) - rifs) / symbol))		#the number still won't be exact because (1) the flav is rounded to 1.28us and (2) the gi is 567 for 2 syms then switches, but it's easily good enough here
			
			print("Computed syms: " + str(syms))
			
			return syms
		elif self.delimtype in [self.DT_SACK, self.DT_RTS_CTS, self.DT_SOUND, self.DT_REVERSE_SOF]:
			debug("Ignoring payload based on delim type {}".format(self.delimtype))
			return -2
		else:
			debug("Unrecognised delim type {}".format(self.delimtype))
			return -1
	
	def getRoboMode(self):
		if self.delimtype == self.DT_BEACON:
			return 2
		elif self.delimtype == self.DT_SOF:
			tmiav = int.from_bytes(self.variant.fields["tmi_av"], byteorder="little", signed=False)
			return tmiav
		else:
			warn("Unrecognised ROBO Mode")
			return -1
	
	def __str__(self):
		dts = "BEACON" if self.delimtype == self.DT_BEACON else "START_OF_FRAME" if self.delimtype == self.DT_SOF else "SACK" if self.delimtype == self.DT_SACK else "RTS_CTS" if self.delimtype == self.DT_RTS_CTS else "SOUND" if self.delimtype == self.DT_SOUND else "REVERSE_SOF" if self.delimtype == self.DT_REVERSE_SOF else "Not implemented"
		return "HomePlug AV Frame Control ({}) for SNID {}".format(dts, self.snid)

class Payload:
	TYPE_BEACON = 0
	TYPE_SOF = 1
	
	PB136_BIT_LENGTH = 136 * 8
	PB520_BIT_LENGTH = 520 * 8

	def __init__(self, payloadtype, bits):
		self.rawbits = bits
		
		#get the PB size (and check it's valid)
		pbsize = len(bits)
		if pbsize not in [self.PB136_BIT_LENGTH, self.PB520_BIT_LENGTH]:
			raise ValueError("Unrecognised PBSize: {}".format(pbsize))
	
		self.fields = {}
		ext = BitsExtractor(bits)
		
		if payloadtype == self.TYPE_BEACON:
			self.fields["nid"] = bitsToBytes(ext.get(54))
			self.fields["hm"] = bitsToBytes(ext.get(2))
			
			self.fields["stei"] = bitsToBytes(ext.get(8))
			self.fields["bt"] = int.from_bytes(bitsToBytes(ext.get(3)), byteorder='little', signed=False)
			self.fields["ncnr"] = bitsToBytes(ext.get(1))
			self.fields["npsm"] = bitsToBytes(ext.get(1))
			self.fields["numslots"] = bitsToBytes(ext.get(3))
			self.fields["slotusage"] = bitsToBytes(ext.get(8))
			self.fields["slotid"] = bitsToBytes(ext.get(3))
			self.fields["aclss"] = bitsToBytes(ext.get(3))
			self.fields["hoip"] = bitsToBytes(ext.get(1))
			self.fields["rtsbf"] = bitsToBytes(ext.get(1))
			self.fields["nm"] = bitsToBytes(ext.get(2))
			self.fields["ccocap"] = bitsToBytes(ext.get(2))
			self.fields["rsvd"] = bitsToBytes(ext.get(4))
			
			self.bmi = BeaconManagementInformation(ext.get(120 * 8))
			self.bpcs = ext.get(32)
		elif payloadtype == self.TYPE_SOF:
			#PHY block header
			self.ssn = int.from_bytes(bitsToBytes(ext.get(16)), byteorder='little', signed=False)
			self.mfbo = int.from_bytes(bitsToBytes(ext.get(9)), byteorder='little', signed=False)
			self.vpbf = bitsToBytes(ext.get(1))
			self.mmqf = bitsToBytes(ext.get(1))
			self.mfbf = int.from_bytes(bitsToBytes(ext.get(1)), byteorder='little', signed=False)
			self.opsf = bitsToBytes(ext.get(1))
			self.rsvd = bitsToBytes(ext.get(3))
			
			self.data = ext.get(pbsize-32-32)
			#self.fields["data"] = bitsToBytes(bits[0:pbsize-32])
			#self.macframe = MACFrame(ext.get(pbsize-32))
			self.bpcs = ext.get(32)						#technically this isn't a bpcs, it's some other name, but fuck it
		else:
			debug("Unimplemented payload type")
		
		if not ext.checkDone():
			debug("WARN: Not all bits extracted in payload")
		
		self.checksumcalc = CRC32ALT().calc(bits[0:pbsize-32])
			
	
	def isChecksumOk(self):
		debug("Calculated Payload CRC32: {}".format(self.checksumcalc))
		debug("Transmitted Payload CRC32: {}".format(list(self.bpcs)))

		return self.checksumcalc == list(self.bpcs)
		


class BeaconManagementInformation:
	def __init__(self, bits):
		self.rawbits = bits

		ext = BitsExtractor(bits)
		
		self.nbe = int.from_bytes(bitsToBytes(ext.get(8)), byteorder='little', signed=False)
		self.be = []
		
		try:
			for i in range(self.nbe):
				hdr = bitsToBytes(ext.get(8))
				length = int.from_bytes(bitsToBytes(ext.get(8)), byteorder='little', signed=False)
				entry = bitsToBytes(ext.get(length * 8))
				self.be.append((hdr, length, entry))
				debug("BMI: {}".format((hdr, length, entry)))
		except Exception:
			warn("Failed to extract BMI")		#just give up
		
		if not ext.checkDone():
			warn("Not all bits extracted in beacon management information")


class PPDU:
	SYNCP_LEN = 384
	HALF_SYNCP_LEN = 192

	preamblelen = 9*384
	hp1fclen = 4*(246+384)+372
	hpavfclen = 1374 + 3072
	paysym1len = 567 + 3072
	paysym2len = 567 + 3072
	paysymxguard = 567
	paysymxlen = paysymxguard + 3072
	
	MINI_ROBO_COPIES = 5
	
	def __init__(self, hpavfc):
		self.avfc = hpavfc
		self.payload = None
	
	def addPayload(self, payload):
		self.payload = payload
		
	def isIgnored(self):
		return self.avfc.delimtype in [AVFrameControl.DT_SACK, AVFrameControl.DT_RTS_CTS, AVFrameControl.DT_SOUND, AVFrameControl.DT_REVERSE_SOF]
	
	def OLD__init__(self, sig, noisevar, channel):
		#store the noise variance			#TODO: this should just come from the env every time, when decoding is all being done in HPAVrx
		self.noisevar = noisevar
		
		self.channel = channel
		
		#separate the FC signal
		self.hp1fc = sig[self.preamblelen:self.preamblelen + self.hp1fclen]
		self.hpavfc = sig[self.preamblelen + self.hp1fclen:self.preamblelen + self.hp1fclen + self.hpavfclen]
		
		#ignore the HP1FC for now
		
		#process the HPAVFC
		hpavfcbits = self.processHPAVFC(self.hpavfc)
		#hpavfcbytes = self.bitsToBytes(hpavfcbits)
		#debug("FC Bytes:\n{}".format(hpavfcbytes))
		self.avfc = AVFrameControl(hpavfcbits)
		
		#check the HPAVFC checksum
		if not self.avfc.isChecksumOk():
			debug("WARN: AV Frame Control Checksum FAILED")
		else:
			debug("FC Checksum OK :)")
		
		#extract values from the HPAVFC
		debug(self.avfc)
		
		#check whether to decode the payload 
		symcount = self.avfc.getSymbolCount()
		if symcount == 0:
			debug("Not proceeding with payload")
			return
			
		
		#separate the payload signal
		self.paysyms = []
		
		paysymstart = self.preamblelen + self.hp1fclen + self.hpavfclen
		if len(self.paysyms) < symcount and paysymstart + self.paysym1len < len(sig):
			self.paysyms.append(sig[paysymstart:(paysymstart+self.paysym1len)])
			paysymstart += self.paysym1len
			
		if len(self.paysyms) < symcount and paysymstart + self.paysym2len < len(sig):
			self.paysyms.append(sig[paysymstart:(paysymstart+self.paysym2len)])
			paysymstart += self.paysym2len
		
		while len(self.paysyms) < symcount and paysymstart + self.paysymxlen < len(sig):
			self.paysyms.append(sig[paysymstart:(paysymstart+self.paysymxlen)])
			paysymstart += self.paysymxlen
		
		self.remainder = sig[paysymstart:len(sig)]
		debug("Remainder after PPDU extraction: {}".format(len(self.remainder)))
		
		#process the body
		payloadbits = self.processPPDUPayload(self.paysyms)
		self.payload = Payload(0, payloadbits)							#TODO: need to dissect based on what type it is
		debug("Payload fields: {}".format(self.payload.fields))
		debug("Payload beacon BMI num entries: {}".format(self.payload.bmi.nbe))
		debug("Payload beacon BMI entries: {}".format(self.payload.bmi.be))
		#debug("Payload beacon BMI bits: {}".format(self.payload.bmi.ALLBITS))
		debug("Payload checksum: {}".format(self.payload.bpcs))
		
		#check the payload checksum
		if not self.payload.isChecksumOk():
			debug("WARN: Payload Checksum FAILED")
		else:
			debug("Payload Checksum OK :D")
		
		
	def processHPAVFC(self, hpavfc):
		#orig = np.array(hpavfc)
		hpavfc = self.restoreCyclicPrefix(hpavfc, 1374)
		
		active = self.countActiveSubcarriers(hpavfc)
		if active != EXPECTED_SUBCARRIER_COUNT:
			debug("WARN: {} Active subcarriers in HPAVFC".format(active))
			
		f = np.fft.fft(hpavfc)[:1536]
		
		#plotTwoSigs(np.abs(f), m2)
		#plotTwoSigs(np.abs(f)[650:1150], m2[650:1150])
		
		#normalise the signal
		f = f / np.real(max(np.abs(f)))
		
		#scale the signal???
		#f = f * np.sqrt(0.5)
		
		#correct for channel
		f = f / self.channel
		
		#use the awful R method of channel correction
		#for i in range(0, len(f)):
			#j = np.complex(0, 1)
			#correc = np.exp(-j * i * -0.0037)
			#f[i] = f[i] * correc
		
		if SIG_DEBUG:
			show(plot(np.real(f), np.imag(f), "ro", label="Channel Corrected Raw Signal"))
		
		#unmap phase mappings
		f = self.unmapPhaseOffsets(f)
		
		if SIG_DEBUG:
			show(plot(np.real(f), np.imag(f), "bo", label="Phase-unwrapped Signal"))
		
		
		# f2 = [f[i] for i in range(len(f)) if i >= usableBW[0] and i <= usableBW[1] and MAIN_TONE_MASK[i]]
		# plot(np.real(f2), np.imag(f2), "bo", label="Channel Corrected Raw Signal")
		# from matplotlib.pyplot import xlim, ylim
		# xlim((-2, 2))
		# ylim((-2, 2))
		# import time
		# filename = "test-results/{}.png".format(int((time.time() % 10000) * 100))
		# debug("Savign as: " + filename)
		# from matplotlib.pyplot import savefig, close
		# savefig(filename)
		# close()
		
		#demod
		#pairs = self.demodulateQPSK(f)
		
		#debug(self.demodulateQPSKwithBW(f))
		#pairs = self.demodulateQPSKwithBW(f)
		pairs = self.softDemodulateQPSKwithBW(f, self.noisevar)
		
		#undiversify
		#interbits = self.reverseFCDiversity(pairs)
		#interbits = self.reverseFCDiversityWithBW(pairs)
		interbits = self.reverseFCDiversityWithSoftBits(pairs)
		debug("Interleaved bits:\n{}".format(interbits))
		
		#channel deinterleave
		(infobits, paritybits) = self.deChannelInterleave(interbits, 128, 128, 4, 16)
		debug("Info bits:\n{}".format(infobits))
		debug("Parity bits:\n{}".format(paritybits))
		
		#skip the error correction
		
		return infobits
	
	def processPPDUPayload(self, paysyms):
		payloadpairs = []
	
		#recover bits from the OFDM symbols
		for i in range(len(paysyms)):
			sym = paysyms[i]
		
			#restore CP
			sym = self.restoreCyclicPrefix(sym, self.paysymxguard)		#TODO: needs to support variable guard lengths
			
			active = self.countActiveSubcarriers(sym)
			if active != EXPECTED_SUBCARRIER_COUNT:
				debug("WARN: {} Active subcarriers in payload symbol {}".format(active, i))
				
			f = np.fft.fft(sym)[:1536]

			#normalise the signal
			f = f / np.real(max(np.abs(f)))
			
			#scale the signal???
			#f = f * np.sqrt(0.5)
			
			#correct for channel
			f = f / self.channel
			
			#TEST: fix SCO
			#sco = -0.0045 / 8 / 6
			#sco = (-0.01736 / 192) * 3072/4446
			#sco = (-0.01736894463524155 / 192) * 3072/4446
			#sco = (-0.02937447337688067 / 192) * 3072/4446
			##sco = (-0.029384728621300534 / 192) * 3072/4446
			#sco = (-0.029364148560748076 / 192) * 3072/4446
			sco = 0
			debug("Using SCO correction: {}".format(sco))
			for sc in range(0, 1536):
				j = np.complex(0, 1)
				correc = np.exp(-j * sc * i * sco)		# * 2 * math.pi   ???
				f[sc] = f[sc] * correc
			
			if SIG_DEBUG:
				show(plot(np.real(f), np.imag(f), "ro", label="Channel Corrected Raw Signal"))
				
			if SIG_DEBUG:
				from matplotlib.pyplot import scatter
				import matplotlib.cm as cm
				import matplotlib.pyplot as plt
				scatter(np.real(f), np.imag(f), c=list(range(0, len(f))), cmap="Greys", label="Raw Signal Colour Mapped")
				plt.colorbar()
				show()
			
			
			#unmap phase mappings
			f = self.unmapPhaseOffsets(f)
			
			if SIG_DEBUG:
				show(plot(np.real(f), np.imag(f), "bo", label="Phase-unwrapped Signal"))
			
			if i == 1000000:
				#f2 = f[usableBW[0]:usableBW[1]]
				#plot(np.real(f), np.imag(f), "bo", label="Channel Corrected Raw Signal")
				f2 = [f[i] for i in range(len(f)) if i >= usableBW[0] and i <= usableBW[1] and MAIN_TONE_MASK[i]]
				plot(np.real(f2), np.imag(f2), "bo", label="Channel Corrected Raw Signal")
				from matplotlib.pyplot import xlim, ylim
				xlim((-2, 2))
				ylim((-2, 2))
				import time
				filename = "test-results/{}p4.png".format(int((time.time() % 10000) * 100))
				debug("Savign as: " + filename)
				from matplotlib.pyplot import savefig, close
				savefig(filename)
				close()
			
			#estimate noise variance
			#noisevar = self.estimateNoiseVariance()
			
			#demod
			#pairs = self.demodulateQPSK(f)				#TODO: why doesn't this use the BW limit??????
			#pairs = self.demodulateQPSKwithBW(f)		#that's better :)
			pairs = self.softDemodulateQPSKwithBW(f, self.noisevar)		#this is even better as it produces LLRs
			
			
			#drop any unused subcarriers
			dropscs = self.calcSubcarriersToDrop(EXPECTED_SUBCARRIER_COUNT, self.MINI_ROBO_COPIES)			#TODO: need to drop any at the end? like in gr-plc?
			pairs = pairs[:-dropscs]
			
			#add to the list of all bit pairs
			payloadpairs += pairs
		
		#unpair the pairs
		robobits = []
		for p in payloadpairs:
			robobits.append(p[0])
			robobits.append(p[1])
		
		debug("Number of ROBO bits: {}".format(len(robobits)))
		
		#combine and recover the message bits
		#debug("All payload pairs:\n{}".format(payloadpairs))
		
		#reverse the ROBO interleaving (as this is HPGP, it's all ROBO)
		rawbits = self.miniRoboDeinterleavewithBWAndSoftBits(robobits)
		
		
		debug("Number of raw bits: {}".format(len(rawbits)))
		
		#channel deinterleave
		(infobits, paritybits) = self.deChannelInterleave(rawbits, 1088, 1088, 16, 136)
		debug("Info bits:\n{}".format(infobits))
		debug("Parity bits:\n{}".format(paritybits))
		
		#skip the error correction
		
		#unscramble
		infobits = self.unscramble(infobits)
		
		debug("Payload length: {}".format(len(infobits)))
		
		return infobits
		
	
	def restoreCyclicPrefix(self, sig, cplen):
		#check synchronisation
		ac = np.correlate(sig[cplen:], sig[:cplen], mode="same") / len(sig)
		expectmax = int(3072 - (cplen / 2))
		if np.argmax(ac) != expectmax:
			debug("WARN: CP does not display expected autocorrelation (exp: {}, act: {})".format(expectmax, np.argmax(ac)))
		
		#remove the cyclic prefix
		cp = sig[:cplen]
		restored = sig[cplen:]
		
		#f = np.fft.fft(cp)
		#plot(np.real(f), np.imag(f), "go", label="CP")
		#f = np.fft.fft(restored[-cplen:])
		#plot(np.real(f), np.imag(f), "bo", label="CP")
		#show()
		#plotTwoSigs(cp, restored[-cplen:])
		f1 = np.fft.fft(cp)
		f2 = np.fft.fft(restored[-cplen:])
		diffs = []
		for i in range(cplen):
			diff = np.angle(np.conj(f1[i]) * f2[i])
			diffs.append(diff)

		mycpoestim = np.mean(diffs) / 3072
		debug("My rolling CPO estim: {}".format(mycpoestim))
		
		#estimate cpo from CP
		#diffs = []
		total = 0
		for i in range(cplen):
			preval = cp[i]
			endval = restored[-cplen+i]
			total = total + (np.conj(preval) * endval)
			#diffs.append(np.angle(np.conj(cp[i]) * restored[-cplen+i]) * 1/cplen)
		#debug("Average CP diff: {}".format(np.mean(diffs)))
		#plotSig(diffs)
		#cpoestim = 1/cplen * np.angle(total)
		cpoestim = np.angle(total) / 3072
		debug("PPDURolling CPO estimate: {}".format(cpoestim))
		
		
		#replace the cyclic prefix in the signal							#TODO: need to correct SCO here first?
		#normalhpavfc = np.array(hpavfc)
		restored[3072-cplen:] = cp
		#plotTwoSigs(hpavfc[2500:], normalhpavfc[2500:])
		#debug("LEN cp: {}".format(len(cp)))
		#debug("LEN hpavfc: {}".format(len(hpavfc)))
	
	
		#correct cpo
		# for i in range(self.HALF_SYNCP_LEN, len(sig)):		#start from the same point as estimation did
			# j = np.complex(0, 1)
			# correc = np.exp(-j * i * cpo)
			# sig[i] = sig[i] * correc
		
		
		#TODO: reinstate this as it seemed to improve reception
		#for i in range(len(restored)):
		#	j = np.complex(0, 1)
		#	correc = np.exp(-j * (3072 * 4 + i) * cpoestim)
		#	restored[i] = restored[i] * correc
	
		return restored
	
	def countActiveSubcarriers(self, sig):
		fp = np.abs(np.fft.fft(sig))
		thresh = np.max(fp) / 2				#np.median finds nearly the minimum value...?
		active = np.ma.count(np.where(fp > thresh))
		
		return active

	"""
	removePhaseOffsets <- function(xffts, init) {
	for (i in seq(1, length(xffts))) {
		if ((i-1) %in% scnums) {
			#print((i-1))
			offset = scphases[i-74] * (pi/4)
			#print(scphases[i-74])
			j = 0+1i
			correc = exp(-j * offset)
			#fcscs[i] = fcscs[i] * correc
			xffts[i] = xffts[i] * correc
		}
	}
	return(xffts)
	"""
	def unmapPhaseOffsets(self, freqs):
		for i in range(0, len(freqs)):
			if MAIN_TONE_MASK[i]:
				offset = mainscphasenums[i - 74] * (math.pi / 4)
				j = np.complex(0, 1)
				correc = np.exp(-j * offset)
				freqs[i] = freqs[i] * correc
		
		return freqs

	"""
	Converts the series of 3072-FFT bins into a QPSK signal. It takes the 1536 positive bins 
	and checks them against the tone mask. If they are active then it interprets the Q channel 
	as the first bit and the I as the second one. 
	It returns a list of these pairs of bits for each unmasked subcarrier. 
	Based on qpskDemod from the R code.
	"""
	def demodulateQPSK(self, freqs):
		dibits = []
		for i in range(0, 1536):
			if MAIN_TONE_MASK[i]:
				#pair = [int(np.imag(freqs[i]) > 0), int(np.real(freqs[i]) > 0)]
				pair = [int(np.real(freqs[i]) > 0), int(np.imag(freqs[i]) > 0)]
				dibits.append(pair)
			#else: dibits.append(["?", "?"])
		
		return dibits
	
	def demodulateQPSKwithBW(self, freqs):
		debug("Limited BW demod")
		
		dibits = []
		for i in range(0, 1536):
			if MAIN_TONE_MASK[i]:
				if i >= usableBW[0] and i <= usableBW[1]:
					#pair = [int(np.imag(freqs[i]) > 0), int(np.real(freqs[i]) > 0)]
					pair = [int(np.real(freqs[i]) > 0), int(np.imag(freqs[i]) > 0)]
					dibits.append(pair)
				else: dibits.append([None, None])
			#else: dibits.append(["x", "x"])
		
		return dibits
	
	def demodulateQPSKwithBWAveraging(self, freqs):
		debug("Limited BW demod with averaging")
		
		dibits = []
		for i in range(0, 1536):
			if MAIN_TONE_MASK[i]:
				if i >= usableBW[0] and i <= usableBW[1]:
					#pair = [int(np.imag(freqs[i]) > 0), int(np.real(freqs[i]) > 0)]
					pair = [np.real(freqs[i]), np.imag(freqs[i])]
					dibits.append(pair)
				else: dibits.append([None, None])
			#else: dibits.append(["x", "x"])
		
		return dibits
	
	"""Soft demodulate using the method described by Rosmansyah (2003) in Soft-Demodulation of QPSK and 
	16-QAM for Turbo Coded WCDMA Mobile Communication Systems  (http://epubs.surrey.ac.uk/792192/1/Rosmansyah2003.pdf)"""
	def softDemodulateQPSKwithBW(self, freqs, noisevar):
		debug("Limited BW soft demod")
		
		root2 = np.sqrt(2)
		
		dibits = []
		for i in range(0, 1536):
			if MAIN_TONE_MASK[i]:
				if i >= usableBW[0] and i <= usableBW[1]:
					y = freqs[i]
					
					p0 = 2/noisevar * np.real(y)/root2		#described as LOG likelihood of '0' over '1', but appears to be '1' over '0'
					p1 = 2/noisevar * np.imag(y)/root2
					
					dibits.append([p0, p1])
				else: dibits.append([None, None])
		
		return dibits
	
	"""
	Reverse the Frame Control diversity copier to return strings of (hopefully) identical bits 
	at pre-determined offsets in the list of pairs. Checks for bit inconsistencies and prints 
	a warning if they are found, but does not take any further action. 
	Based on undiversify in the R code. 
	"""
	def reverseFCDiversity(self, pairs):
		bitreplicas = [[] for x in range(0, 256)]		#start with *separate* empty lists
		for i in range(0, len(pairs)):
			iaddr = i % 256
			qaddr = (i + 128) % 256
			bitreplicas[iaddr].append(pairs[i][1])		#bits are arranged (Q-channel, I-channel)
			bitreplicas[qaddr].append(pairs[i][0])
		
		allcorrect = self.testUndiversifiedBits(bitreplicas)
		if not allcorrect:
			debug("WARN: Inconsistent bits found in FC diversity")							#TODO: indicate error somehow and/or attempt to correct
		
		origbits = []
		for i in range(0, len(bitreplicas)):
			origbits.append(bitreplicas[i][0])
		
		return origbits
	
	def testUndiversifiedBits(self, bitreplicas):
		allcorrect = True
		for i in range(0, len(bitreplicas)):
			tot = sum(bitreplicas[i])
			if tot != 0 and tot != len(bitreplicas[i]):
				debug("{}: {}".format(i, bitreplicas[i]))
				allcorrect = False
		
		return allcorrect
	
	def reverseFCDiversityWithBW(self, pairs):
		debug("Limited BW FC diversity")
	
		bitreplicas = [[] for x in range(0, 256)]		#start with *separate* empty lists
		for i in range(0, len(pairs)):
			iaddr = i % 256
			qaddr = (i + 128) % 256
			#bitreplicas[iaddr].append(pairs[i][1])		#bits are arranged (Q-channel, I-channel)
			#bitreplicas[qaddr].append(pairs[i][0])
			bitreplicas[iaddr].append(pairs[i][0])		#NO THEY AREN'T
			bitreplicas[qaddr].append(pairs[i][1])
		
		#allcorrect = self.testUndiversifiedBits(bitreplicas)
		#if not allcorrect:
			#debug("WARN: Inconsistent bits found in FC diversity")							#TODO: indicate error somehow and/or attempt to correct
		
		origbits = []
		for i in range(0, len(bitreplicas)):
			reps = bitreplicas[i]
			reps = list(filter(lambda x: x != None, reps))
			bit = int(np.round(np.mean(reps)))		#np.round will select 0 if there are even votes each way
			origbits.append(bit)
		
		return origbits

	def reverseFCDiversityWithSoftBits(self, pairs):
		debug("Limited BW FC diversity")
	
		bitreplicas = [[] for x in range(0, 256)]		#start with *separate* empty lists
		for i in range(0, len(pairs)):
			iaddr = i % 256
			qaddr = (i + 128) % 256
			bitreplicas[iaddr].append(pairs[i][0])
			bitreplicas[qaddr].append(pairs[i][1])
		
		#allcorrect = self.testUndiversifiedBits(bitreplicas)
		#if not allcorrect:
			#debug("WARN: Inconsistent bits found in FC diversity")							#TODO: indicate error somehow and/or attempt to correct
		
		origbits = []
		for i in range(0, len(bitreplicas)):
			reps = bitreplicas[i]
			reps = list(filter(lambda x: x != None, reps))
			bit = int(np.mean(reps) > 0)
			origbits.append(bit)
		
		return origbits
	
	"""
	deChannelInterleave <- function(bits, infobitcount, paritybitcount, stepsize, parityoffset) {
		#some precondition checks
		if (infobitcount == 128 && paritybitcount == 128 && stepsize == 4 && parityoffset == 16) {  }	#ok
		else if (infobitcount == 1088 && paritybitcount == 1088 && stepsize == 16 && parityoffset == 136) {  }	#ok
		else {
			stop("Unsupported size, only PB16 & PB136 atm")
		}

		#reverse sub-bank switching
		subbankbitunswitch <- function(nib, nibnum) {
			switchmode = (nibnum %% 8) + 1
			if (switchmode == 1 || switchmode == 2) { return(nib) }
			else if (switchmode == 3 || switchmode == 4) { return(c(nib[4], nib[1:3])) }
			else if (switchmode == 5 || switchmode == 6) { return(c(nib[3], nib[4], nib[1:2]))  }
			else if (switchmode == 7 || switchmode == 8) { return(c(nib[2], nib[3], nib[4], nib[1])) }
			else { stop("Nibble number error") }
		}

		infocolsize = infobitcount/4
		paritycolsize = paritybitcount/4

		#read the interleaved bits out into the sub-banks (as nibbles)
		subbanki = matrix(NA, infocolsize, 4)	
		subbankp = matrix(NA, paritycolsize, 4)

		inforow = 0
		parityrow = 0
		for (i in seq(0, (length(bits)/4)-1)) {
			nibstart = (i*4)+1
			nibend = (i*4)+4
			nibble = bits[nibstart:nibend]
			isparity = as.logical(i %% 2)		#nibbles are interleaved (info, parity, info, parity etc.)

			if (isparity) {
				subbankp[(((parityrow+parityoffset)%%paritycolsize)+1),] = subbankbitunswitch(nibble, i)
				parityrow = parityrow + stepsize
				if (parityrow >= paritycolsize) { parityrow = (parityrow %% paritycolsize) + 1 }
			}
			else {
				subbanki[(inforow+1),] = subbankbitunswitch(nibble, i)
				inforow = inforow + stepsize
				if (inforow >= infocolsize) { inforow = (inforow %% infocolsize) + 1 }
			}
		}

		deinterleaveinfo = c(subbanki[,1], subbanki[,2], subbanki[,3], subbanki[,4])
		deinterleaveparity = c(subbankp[,1], subbankp[,2], subbankp[,3], subbankp[,4])

		return(cbind(deinterleaveinfo, deinterleaveparity))
	}
	"""
	def deChannelInterleave(self, bits, infobitcount, paritybitcount, stepsize, parityoffset):
		infocolsize = int(infobitcount / 4)		#always neatly divisible by four, so this is just a typecast
		paritycolsize = int(paritybitcount / 4)
		
		#subbanki = [None for i in range(0, infocolsize)]
		#subbankp = [None for i in range(0, paritycolsize)]
		
		# subbanki = [np.zeros(infocolsize), np.zeros(infocolsize), np.zeros(infocolsize), np.zeros(infocolsize)]
		# subbankp = [np.zeros(paritycolsize), np.zeros(paritycolsize), np.zeros(paritycolsize), np.zeros(paritycolsize)]
		subbanki = [np.full(infocolsize, -1), np.full(infocolsize, -2), np.full(infocolsize, -3), np.full(infocolsize, -4)]
		subbankp = [np.full(paritycolsize, -1), np.full(paritycolsize, -2), np.full(paritycolsize, -3), np.full(paritycolsize, -4)]
		
		#read in by row
		inforow = 0
		parityrow = 0
		for i in range(0, int(len(bits)/4)):
			nibstart = i*4
			nibend = i*4 + 4
			nibble = bits[nibstart:nibend]
			isparity = i % 2 != 0
			
			if isparity:
				rowindex = (parityrow + parityoffset) % paritycolsize
				unswitched = self.subBankBitUnswitch(nibble, i)
				subbankp[0][rowindex] = unswitched[0]
				subbankp[1][rowindex] = unswitched[1]
				subbankp[2][rowindex] = unswitched[2]
				subbankp[3][rowindex] = unswitched[3]
				parityrow += stepsize
				if parityrow >= paritycolsize:
					parityrow = (parityrow % paritycolsize) + 1
			else:
				unswitched = self.subBankBitUnswitch(nibble, i)
				subbanki[0][inforow] = unswitched[0]
				subbanki[1][inforow] = unswitched[1]
				subbanki[2][inforow] = unswitched[2]
				subbanki[3][inforow] = unswitched[3]
				inforow += stepsize
				if inforow >= infocolsize:
					inforow = (inforow % infocolsize) + 1
		
		#read out by column
		info = np.concatenate([subbanki[0], subbanki[1], subbanki[2], subbanki[3]])
		parity = np.concatenate([subbankp[0], subbankp[1], subbankp[2], subbankp[3]])
		
		#test the deinterleaving
		if not self.testChannelDeinterleave(info, parity):
			debug("WARN: Invalid values in deinterleave")
		
		return (info, parity)
	
	def subBankBitUnswitch(self, nibble, nibnum):
		switchmode = (nibnum % 8) + 1
		if switchmode == 1 or switchmode == 2: return nibble
		elif switchmode == 3 or switchmode == 4: return [nibble[3]] + nibble[:3]
		elif switchmode == 5 or switchmode == 6: return nibble[2:] + nibble[0:2]
		elif switchmode == 7 or switchmode == 8: return nibble[1:] + [nibble[0]]
		else: raise Exception("Nibble number error")
	
		return nibble
		
	def testChannelDeinterleave(self, info, parity):
		return min(info) >= 0 and min(parity) >= 0
	
	"""
	Based on the ROBO parameters calculation, this determines how many subcarriers need to be dropped 
	to come down to the ncarrierrobo number.
	"""
	def calcSubcarriersToDrop(self, ncarrier, ncopies):
		ncarrierrobo = ncopies * math.floor(ncarrier / ncopies)
		return ncarrier - ncarrierrobo
	
	"""
	minirobodeinterleave <- function(robobits) {
		#prepare the bit replicas buffer
		payloadbitcopies = matrix(NA, 5, 2176)						#appears from autocorrelation that the data are actually repeated every 2196, but this is because of 20 value padding which is discarded here

		nraw = 136 * 8 * 2		#PB136 * bitstobytes * 2 from coding
		ncarrier = 917
		ncopies = 5		#MINI-ROBO
		bpc = 2			#QPSK

		ncarrierrobo = ncopies * floor(ncarrier/ncopies)
		carriersinsegment = ncarrierrobo/ncopies
		bitspersymbol = bpc * ncarrierrobo
		bitsinsegment = bpc * carriersinsegment
		bitsinlastsymbol = nraw - bitspersymbol * floor(nraw / bitspersymbol)

		if (bitsinlastsymbol == 0) {
			bitsinlastsymbol = bitspersymbol
			bitsinlastsegment = bitsinsegment
		}
		else {
			bitsinlastsegment = bitsinlastsymbol - bitsinsegment * floor((bitsinlastsymbol -1) / bitsinsegment)
		}
		npad = bitsinsegment - bitsinlastsegment

		#cyclic shift for MINIROBO only
		if (bitsinlastsymbol <= 4 * bitsinsegment) {
			cyclicshift = c(0,0,0,0,0)
		}
		else {
			cyclicshift = c(0,1,2,3,4)
		}

		#reorder the values
		for (k in seq(0, ncopies-1)) {
			if (cyclicshift[k+1] == 0) {
				for (i in seq(1, nraw)) {
					payloadbitcopies[(k+1),i] = robobits[(i+k*(nraw+npad))]
				}
				for (i in seq(1, npad)) {
					payloadbitcopies[(k+1),i] = robobits[(nraw+i+k*(nraw+npad))]
				}
			}
			else if (cyclicshift[k+1] > 0) {
				numberbitsshifted = (cyclicshift[k+1]-1) * bitsinsegment + bitsinlastsegment
				for (i in seq(1, numberbitsshifted)) {
					payloadbitcopies[(k+1), (nraw - numberbitsshifted + i)] = robobits[(i+k*(nraw+npad))]
				}
				for (i in seq(1, npad)) {
					payloadbitcopies[(k+1),i] = robobits[(i+numberbitsshifted+k*(nraw+npad))]
				}
				for (i in seq(1, (nraw - numberbitsshifted))) {
					payloadbitcopies[(k+1),i] = robobits[(i+numberbitsshifted+npad+k*(nraw+npad))]
				}
			}
			else {stop("Invalid cyclic shift")}
		}

		return(payloadbitcopies)
	}
	"""
	def miniRoboDeinterleave(self, bits):	
		nraw = 136 * 8 * 2		#PB136 * bitstobytes * 2 from coding
		ncarrier = 917
		ncopies = 5		#MINI-ROBO
		bpc = 2			#QPSK
		
		#define the output buffer
		deinterleaved = [[] for i in range(ncopies)]
		padding = [[] for i in range(ncopies)]
		
		#calculate number of bits to pad
		ncarrierrobo = ncopies * math.floor(ncarrier / ncopies)
		carriersinsegment = math.floor(ncarrierrobo / ncopies)
		bitspersymbol = bpc * ncarrierrobo
		bitsinsegment = bpc * carriersinsegment
		bitsinlastsymbol = nraw - bitspersymbol * math.floor(nraw / bitspersymbol)
	
		if bitsinlastsymbol == 0:
			bitsinlastsymbol = bitspersymbol
			bitsinlastsegment = bitsinsegment
		else:
			bitsinlastsegment = bitsinlastsymbol - bitsinsegment * math.floor((bitsinlastsymbol - 1) / bitsinsegment)

		npad = bitsinsegment - bitsinlastsegment
	
		#calculate cyclic shift
		if ncopies == 2:
			raise Exception("Unimplemented")
		elif ncopies == 4:
			raise Exception("Unimplemented")
		elif ncopies == 5:
			#MINI-ROBO
			if bitsinlastsymbol <= 4 * bitsinsegment:
				cyclicshift = [0,0,0,0,0]
			else:
				cyclicshift = [0,1,2,3,4]
		else:
			raise Exception("Unknown number of copies in ROBO deinterleave")
		
		#assign output of the robo interleaver
		for k in range(ncopies):
			if cyclicshift[k] == 0:
				#recover the bits
				for i in range(nraw):
					deinterleaved[k].append(bits[k * (nraw + npad) + i])
				
				#recover the padding
				for i in range(npad):
					padding[k].append(bits[k * (nraw + npad) + nraw + i])
			elif cyclicshift[k] > 0:
				numberbitsshifted = (cyclicshift[k] - 1) * bitsinsegment + bitsinlastsegment
				
				raise Exception("Unimplemented")
			else:
				raise Exception("Invalid cyclic shift (<0?)")
		
		if not self.testMiniRoboDeinterleave(deinterleaved, padding):
			debug("WARN: Inconsistencies in MINI-ROBO deinterleaved bits")
		
		#TODO: combine together in a more beneficial way
		deintersample = list(zip(*deinterleaved))
		deintersample = list(map(sum, deintersample))
		debug("Deinterleaved sample:\r\n{}".format(deintersample))
		
		return deinterleaved[0]
	
	def testMiniRoboDeinterleave(self, deinterleaved, padding):
		#test whether all the deinterleaved copies match
		bitsconsistent = True
		d0 = deinterleaved[0]
		for i in range(1, len(deinterleaved)):
			if deinterleaved[i] != d0:
				bitsconsistent = False
	
		#test whether all the padding copies match
		paddingconsistent = True
		p0 = padding[0]
		for i in range(1, len(padding)):
			if padding[i] != p0:
				paddingconsistent = False
		
		#test whether the padding duplicates the first n deinterleaved bits
		paddingmatchesstart = True
		for i in range(len(padding)):
			npad = len(padding[i])
			matchedbits = deinterleaved[i][:npad]
			if padding[i] != matchedbits:
				paddingmatchesstart = False
		
		return bitsconsistent and paddingconsistent and paddingmatchesstart
		
	"""A development of the Mini-ROBO deinterleave function that combines soft bits by arithmetic mean"""
	def miniRoboDeinterleavewithBWAndSoftBits(self, bits):	
		nraw = 136 * 8 * 2		#PB136 * bitstobytes * 2 from coding
		ncarrier = 917
		ncopies = 5		#MINI-ROBO
		bpc = 2			#QPSK
		
		#define the output buffer
		deinterleaved = [[] for i in range(ncopies)]
		padding = [[] for i in range(ncopies)]
		
		#calculate number of bits to pad
		ncarrierrobo = ncopies * math.floor(ncarrier / ncopies)
		carriersinsegment = math.floor(ncarrierrobo / ncopies)
		bitspersymbol = bpc * ncarrierrobo
		bitsinsegment = bpc * carriersinsegment
		bitsinlastsymbol = nraw - bitspersymbol * math.floor(nraw / bitspersymbol)
	
		if bitsinlastsymbol == 0:
			bitsinlastsymbol = bitspersymbol
			bitsinlastsegment = bitsinsegment
		else:
			bitsinlastsegment = bitsinlastsymbol - bitsinsegment * math.floor((bitsinlastsymbol - 1) / bitsinsegment)

		npad = bitsinsegment - bitsinlastsegment
	
		#calculate cyclic shift
		if ncopies == 2:
			raise Exception("Unimplemented")
		elif ncopies == 4:
			raise Exception("Unimplemented")
		elif ncopies == 5:
			#MINI-ROBO
			if bitsinlastsymbol <= 4 * bitsinsegment:
				cyclicshift = [0,0,0,0,0]
			else:
				cyclicshift = [0,1,2,3,4]
		else:
			raise Exception("Unknown number of copies in ROBO deinterleave")
		
		#assign output of the robo interleaver
		for k in range(ncopies):
			if cyclicshift[k] == 0:
				#recover the bits
				for i in range(nraw):
					deinterleaved[k].append(bits[k * (nraw + npad) + i])
				
				#recover the padding
				for i in range(npad):
					padding[k].append(bits[k * (nraw + npad) + nraw + i])
			elif cyclicshift[k] > 0:
				numberbitsshifted = (cyclicshift[k] - 1) * bitsinsegment + bitsinlastsegment
				
				raise Exception("Unimplemented")
			else:
				raise Exception("Invalid cyclic shift (<0?)")
		
		#remove the None values and combine the various probabilities using arithmetic mean
		bitreplicas = list(zip(*deinterleaved))
		combined = []
		for i in range(0, len(bitreplicas)):
			reps = bitreplicas[i]
			reps = list(filter(lambda x: x != None, reps))
			bit = np.mean(reps) > 0
			combined.append(bit)
		
		return combined
	
	"""
	unscramble <- function(bits) {
		x10 = c(1,1,1,1,1,1,1)
		x3 = c(1,1,1)
		#x0 = c(1)

		outbits = vector(length=length(bits))
		for (i in seq(1, length(bits))) {
			bit = bits[i]
			x10_3 = xor(x10[1], x3[1])
			#x3_0???????
			x10 = shiftin(x10, x3[1])
			newbit = xor(bit, x10_3)
			#x3 = shiftin(x3, bit)
			x3 = shiftin(x3, x10_3)
			#outbits[i] = xor(bit, x10_3)
			outbits[i] = newbit
			##x3 = shiftin(x3, x0)
			##x0 = shiftin(x0, x10_3)
			#outbits[i] = xor(bit, x10_3)
		}
		return(outbits)
	}
	"""
	def unscramble(self, bits):
		x10 = [1,1,1,1,1,1,1]
		x3 = [1,1,1]
		
		outbits = []
		
		for i in range(len(bits)):
			bit = bits[i]
			
			x10_3 = x10[0] ^ x3[0]
			x10 = self.shiftin(x10, x3[0])
			newbit = bit ^ x10_3
			x3 = self.shiftin(x3, x10_3)
			outbits.append(newbit)
		
		return outbits
			
	def shiftin(self, existing, new):
		existing.pop(0)
		return existing + [new]