from matplotlib.pyplot import plot, show
import numpy as np
import struct

import glob
import os

from . import config

package_directory = os.path.dirname(os.path.abspath(__file__))				#hack to make the ref-data directory relative despite being in a package now

def debug(msg):
	print(msg)

def warn(msg):
	print("WARN: " + str(msg))

def debugPlot(vals, text):
	if config.SIG_DEBUG:
		debug("Showing figure: {}".format(text))
		show(plot(vals))

def initResultsDir():
	if not os.path.exists(config.resultsdir):
		os.makedirs(config.resultsdir)

def plotSig(sig):
	show(plot(sig))

def plotTwoSigs(sig1, sig2):
	plot(sig1)
	plot(sig2)
	show()

class BitsExtractor:
	def __init__(self, bits):
		self.bits = bits
		self.i = 0
		self.max = len(bits)
	
	def get(self, n):
		if self.i + n > self.max:
			raise Exception("More bits requested than exist")
		
		val = self.bits[self.i:(self.i+n)]
		self.i += n
		return val
	
	def rest(self):
		i = self.i
		self.i = self.max
		return self.bits[i:]
	
	def checkDone(self):
		return self.i == self.max

class BytesExtractor:
	def __init__(self, by):
		self.by = by
		self.i = 0
		self.max = len(by)
	
	def get(self, n):
		if self.i + n > self.max:
			raise Exception("More bytes requested than exist")
		
		val = self.by[self.i:(self.i+n)]
		self.i += n
		return val
	
	def rest(self):
		i = self.i
		self.i = self.max
		return self.by[i:]
	
	def checkDone(self):
		return self.i == self.max


def bitsToBytes(bits):
	#sanity checks
	if bits is None or len(bits) == 0:
		return []
	
	if min(bits) < 0 or max(bits) > 1:
		raise Exception("Input bit array has non-binary values")

	numbytes = int(len(bits) / 8)
	roundlen = numbytes * 8
	overhang = len(bits) % 8
	
	bytes = bytearray(numbytes + 1) if overhang > 0 else bytearray(numbytes)
	
	#iterate in REVERSE ORDER so the bytes can be filled MSB->LSB
	for i in range(roundlen - 1, -1, -1):
		b = int(i / 8)
		bytes[b] = (bytes[b] << 1) + bits[i]
	
	for i in range(roundlen + overhang - 1, roundlen - 1, -1):
		b = int(i / 8)
		bytes[b] = (bytes[b] << 1) + bits[i]
	
	return bytes

def bitsToReverseBytes(bits):
	#sanity check
	if min(bits) < 0 or max(bits) > 1:
		raise Exception("Input bit array has non-binary values")

	numbytes = int(len(bits) / 8)
	roundlen = numbytes * 8
	overhang = len(bits) % 8
	
	bytes = bytearray(numbytes + 1) if overhang > 0 else bytearray(numbytes)
	
	#iterate in REVERSE ORDER so the bytes can be filled MSB->LSB
	for i in range(roundlen):
		b = int(i / 8)
		bytes[b] = (bytes[b] << 1) + bits[i]
	
	for i in range(roundlen, roundlen + overhang):
		b = int(i / 8)
		bytes[b] = (bytes[b] << 1) + bits[i]
	
	return bytes

def bytesToBits(bys):
	bits = []
	
	for b in bys:
		for i in range(8):
			bits.append(b & 0x01)
			b = b >> 1
	
	return bits

"""
Loads a standard R 'floats' file, as in one output by the write() function with real values.
"""
def loadRFileFloats(filename):
	vals = []
	inf = open(filename, "r")
	for l in inf:
		l = l.strip()
		nums = l.split(" ")
		for n in nums:
			vals.append(float(n))
	inf.close()
	
	return np.array(vals)

"""
Load a raw binary file with complex signal values. Uses numpy instead of a manual method for big performance benefits. 
"""
def loadRawComplex(filename):
	return np.fromfile(filename, dtype=np.complex64)	#default complex is complex128, but our values are not

"""
Load a raw binary file with real signal values
"""
def loadRawFloat(filename):
	with open(filename, "rb") as inf:
		vals = []
		more = True
		while more:
			v = inf.read(4)
			if len(v) == 4:
				re = struct.unpack('f', v)[0]
				vals.append(re)
			else:
				more = False
				
	return vals

"""Uses glob to get Tagged File Sink files in a path and then sorts them by the filenumber in the middle"""
def sortedGlob(path):
	unordered = glob.glob(path)
	pairs = []
	for u in unordered:
		one = u.find("_")
		two = u.find("_", one + 1)
		num = int(u[one+1:two])
		pairs.append((num, u))
	
	#return list(zip(*sorted(pairs)))[1]
	return sorted(pairs)