from .utils import *
from . import HPAVrxphy
from .HPAVrxphy import HPAVRXPHY
#import HPAVrxmac
from .HPAVrxmac import HPAVRXMAC
#import PPDU
from .PPDU import *
#import MPDU
from .MPDU import *


class RXResult():
	def __init__(self, filename):
		self.filename = filename
		self.success = None
		self.ignore = False
		self.statusmsg = None
		self.ppdu = None
		self.mpdu = None
		self.keys = None
		self.debugvals = None
	
	def error(self, msg, trace = None, debugvals = None):
		self.success = False
		self.statusmsg = msg
		self.trace = trace
		self.debugvals = debugvals

	def ignored(self, msg, debugvals = None):
		self.success = True
		self.ignore = True
		self.statusmsg = msg
		self.debugvals = debugvals
		
	def complete(self, ppdu, debugvals = None):
		self.success = True
		self.statusmsg = "Complete"
		self.ppdu = ppdu
		self.debugvals = debugvals
	
	def addMPDU(self, mpdu):
		self.mpdu = mpdu
		
	def addKeys(self, keys):
		self.keys = keys
	
	def __str__(self):
		return "RXResult ({}), err:{}, PPDU:{}".format(self.success, self.statusmsg, self.ppdu)
	

class HPAVRX:
	def __init__(self, sessionname="test", phyrxenv=HPAVrxphy.RXEnv()):
		self.phy = HPAVRXPHY(rxenv=phyrxenv)
		self.mac = HPAVRXMAC(sessionname)
	
	def rx(self, src):
		result = RXResult(src.filename)
		
		try:
			ppdu = self.phy.rx(src)
			
			if ppdu.isIgnored():
				result.ignored("PPDU of type {} ignored".format(ppdu.avfc.delimtype))
			else:
				macresult = self.mac.handle(ppdu)

				result.complete(ppdu, self.phy.rxenv.debugvals)
				
				if macresult is not None:
					(mpdu, keys) = macresult
					result.addMPDU(mpdu)
					result.addKeys(keys)
		
		except Exception as e:
			#result.error(str(e), self.phy.rxenv.debugvals)
			raise e
		
					
		return result