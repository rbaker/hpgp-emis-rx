from .utils import *
import binascii

class MACFrame:
	def __init__(self, bits):		
		self.rawbits = bits
		
		debug("Length of MAC Frame: {} ({})".format(len(bits), len(bits)/8))
		
		ext = BitsExtractor(bits)
		
		#MAC body header
		self.mftype = int.from_bytes(bitsToBytes(ext.get(2)), byteorder='little', signed=False)
		self.mflength = int.from_bytes(bitsToBytes(ext.get(14)), byteorder='little', signed=False) + 1			#Spec 4.3.1.2 notes that the length is one more than this value (includes confounder/ats but excludes header and ICV)
		
		self.mm = None
		self.macpayload = None
		
		print("MAC Frame Type: {}".format(self.mftype))
		print("MAC Frame Length: {}".format(self.mflength))
		
		if self.mftype == 3:
			self.confounder = bitsToBytes(ext.get(4 * 8))
			self.mm = ManagementMessage(bitsToBytes(ext.get((self.mflength - 4) * 8)))
			
		if self.mftype == 2:
			self.ats = bitsToBytes(ext.get(4*8))
			#self.macpayload = bitsToBytes(ext.get(len(bits) - (2*8 + 4*8 + 4*8)))		#may not be mflength as MPDU might span PPDUs
			self.macpayload = bitsToBytes(ext.get(self.mflength * 8))
			
		if self.mftype == 1:
			#self.macpayload = bitsToBytes(ext.get(len(bits) - (2*8 + 4*8)))		#may not be mflength as MPDU might span PPDUs
			self.macpayload = bitsToBytes(ext.get(self.mflength * 8))

		self.icv = bitsToBytes(ext.get(4 * 8))
		
		print("REMAINDER: " + str(bitsToBytes(ext.rest())))
		
		if not ext.checkDone():
			debug("WARN: Not all bits extracted in mac frame")



class ManagementMessage():
	def __init__(self, by):
		self.rawbytes = by
		ext = BytesExtractor(by)
		
		#Management message header
		self.oda = ext.get(6)
		self.osa = ext.get(6)
		#self.vlantag = ext.get(4)		#optional (but present so far in observed)
		
		self.mtype = ext.get(2)		#0x88e1
		self.mmv = ext.get(1)
		self.mmtype = ext.get(2)			#parsed as a multi-byte number so order needs to be reversed if using raw (or parsing routines handle it)
		self.fmi = ext.get(2)		#TODO: FMI is split into three bits
		
		#generic dictionary of values for specific messages
		self.values = dict()
		
		#print("ODA: {}".format(self.oda))
		#print("OSA: {}".format(self.osa))
		#print("Ethertype: {}".format(self.mtype))
		#print("MMV: {}".format(self.mmv))
		#print("MM: {}".format(self.mmtype))
		#print("MM (parse): {}".format(hex(int.from_bytes(self.mmtype, byteorder='little', signed=False))))
		
		self.mme = None
		
		#CM_SLAC_PARM.REQ
		if int.from_bytes(self.mmtype, byteorder='little', signed=False) == 0x6064:
			self.mme = MM_CM_SLAC_Parm_Req(ext.rest(), self.values)
		
		#CM_SLAC_PARM.CNF
		if int.from_bytes(self.mmtype, byteorder='little', signed=False) == 0x6065:		#not 0x6068, as suggested in the spec's table (rather it follows the .CNF = +1 rule)
			self.mme = MM_CM_SLAC_Parm_Cnf(ext.rest(), self.values)
		
		#CM_SLAC_MATCH.CNF
		if int.from_bytes(self.mmtype, byteorder='little', signed=False) == 0x607D:		#according to the spec this is CM_VALIDATE.CNF, but it appears to actually be CM_SLAC_MATCH.CNF
			self.mme = MM_CM_SLAC_Match(ext.rest(), self.values)
		
		#CM_ENCRYPTED_PAYLOAD
		if int.from_bytes(self.mmtype, byteorder='little', signed=False) == 0x6006:
			self.mme = MM_CM_Encrypted_Payload(ext.rest(), self.values)
		
		#CM_GET_KEY
		if int.from_bytes(self.mmtype, byteorder='little', signed=False) == 0x600D:
			self.mme = MM_CM_Get_Key(ext.rest(), self.values)
			
		if not ext.checkDone():
			debug("WARN: Not all bytes extracted in management message")
	
	
	def getMMName(self):
			mmtypenum = int.from_bytes(self.mmtype, byteorder='little', signed=False)
		
			names = {
				0x6064:	"CM_SLAC_PARM.REQ",
				0x6065:	"CM_SLAC_PARM.CNF",
				0x606A: "CM_START_ATTEN_CHAR.IND",
				0x6070: "CM_PKCS_CERT.REQ",
				0x6076: "CM_MNBC_SOUND.IND",
				0x606E: "CM_ATTEN_CHAR.IND",
				0x606F: "CM_ATTEN_CHAR.RSP",
				0x607C: "CM_SLAC_MATCH.REQ",
				0x607D: "CM_SLAC_MATCH.CNF",
				0x6080: "CM_SLAC_USER_DATA.REQ",
				0x6081: "CM_SLAC_USER_DATA.CNF",
				0x6006: "CM_ENCRYPTED_PAYLOAD",
				0x600D: "CM_GET_KEY"
			}
			
			if mmtypenum in names:
				return names[mmtypenum]
			else:
				return "Unknown"
		

class MM_CM_SLAC_Parm_Req():
	def __init__(self, by, vals):
		print("BYTES: " + str(by))
		
		ext = BytesExtractor(by)
		
		#MME
		self.apptype = ext.get(1)
		self.sectype = ext.get(1)
		self.runid = ext.get(8)
		if int.from_bytes(self.sectype, byteorder='little', signed=False) != 0x00:
			self.ciphersuitesize = int.from_bytes(ext.get(1), byteorder='little', signed=False)
			self.ciphersuite = []
			for i in range(self.ciphersuitesize):
				self.ciphersuite.append(ext.get(2))
		
		#store the values generically
		valskey = "MM_CM_SLAC_Parm_Req"
		vals[valskey + ".apptype"] = self.apptype
		vals[valskey + ".sectype"] = self.sectype
		vals[valskey + ".runid"] = self.runid
		if int.from_bytes(self.sectype, byteorder='little', signed=False) != 0x00:
			vals[valskey + ".ciphersuitesize"] = self.ciphersuitesize
			for i in range(self.ciphersuitesize):
				vals[valskey + ".ciphersuite" + str(i)] = self.ciphersuite[i]
		
		if not ext.checkDone():
			debug("WARN: Not all bytes extracted in CM_SLAC_Parm_Req")


class MM_CM_SLAC_Parm_Cnf():
	def __init__(self, by, vals):
		print("BYTES: " + str(by))
		
		ext = BytesExtractor(by)
		
		#MME
		self.msoundtarget = ext.get(6)
		self.numsounds = ext.get(1)
		self.timeout = ext.get(1)
		self.resptype = ext.get(1)
		self.fwdsta = ext.get(6)
		self.apptype = ext.get(1)
		self.sectype = ext.get(1)
		self.runid = ext.get(8)
		self.ciphersuite = ext.get(2)
		
		#store the values generically
		valskey = "MM_CM_SLAC_Parm_Cnf"
		vals[valskey + ".msoundtarget"] = self.msoundtarget
		vals[valskey + ".resptype"] = self.resptype
		vals[valskey + ".apptype"] = self.apptype
		vals[valskey + ".sectype"] = self.sectype
		vals[valskey + ".runid"] = self.runid
		vals[valskey + ".ciphersuite"] = self.ciphersuite
		
		
		if not ext.checkDone():
			debug("WARN: Not all bytes extracted in CM_SLAC_Parm_Req")

			
class MM_CM_SLAC_Parm_Cnf2():
	def __init__(self, by, vals):
		print("BYTES: " + str(by))
		
		ext = BytesExtractor(by)
		
		#MME
		self.msoundtarget = ext.get(6)
		self.numsounds = ext.get(1)
		self.timeout = ext.get(1)
		self.resptype = ext.get(1)
		self.fwdsta = ext.get(6)
		self.apptype = ext.get(1)
		self.sectype = ext.get(1)
		self.runid = ext.get(8)
		self.ciphersuite = ext.get(2)
		
		#store the values generically
		valskey = "MM_CM_SLAC_Parm_Cnf2"
		vals[valskey + ".msoundtarget"] = self.msoundtarget
		vals[valskey + ".resptype"] = self.resptype
		vals[valskey + ".apptype"] = self.apptype
		vals[valskey + ".sectype"] = self.sectype
		vals[valskey + ".runid"] = self.runid
		vals[valskey + ".ciphersuite"] = self.ciphersuite
		
		
		if not ext.checkDone():
			debug("WARN: Not all bytes extracted in CM_SLAC_Parm_Req")


class MM_CM_SLAC_Match():
	def __init__(self, by, vals):
		print("BYTES: " + str(by))
		
		ext = BytesExtractor(by)
		
		#MME
		self.apptype = ext.get(1)
		self.sectype = ext.get(1)
		self.mvflen = ext.get(2)
		self.mvflength = int.from_bytes(self.mvflen, byteorder='little', signed=False)
		#self.var = ext.get(self.mvflength)
		
		if int.from_bytes(self.sectype, byteorder='little', signed=False) != 0x00:
			warn("Security set for SLAC_MATCH.CNF")
		
		#parse the var field (assuming no security)
		self.pevid = ext.get(17)
		self.pevmac = ext.get(6)
		self.evseid = ext.get(17)
		self.evsemac = ext.get(6)
		self.runid = ext.get(16)
		self.nid = ext.get(8)
		
		print("PEV ID: {}".format(self.pevid))
		print("PEV MAC: {}".format(self.pevmac))
		print("EVSE ID: {}".format(self.evseid))
		print("EVSE MAC: {}".format(self.evsemac))
		print("RUN ID: {}".format(self.runid))
		print("NID: {}".format(self.nid))
		
		self.nmk = ext.get(16)
		
		#store the values generically
		valskey = "MM_CM_SLAC_Match"
		vals[valskey + ".apptype"] = self.apptype
		vals[valskey + ".sectype"] = self.sectype
		vals[valskey + ".pevid"] = self.pevid
		vals[valskey + ".pevmac"] = self.pevmac
		vals[valskey + ".evseid"] = self.evseid
		vals[valskey + ".evsemac"] = self.evsemac
		vals[valskey + ".runid"] = self.runid
		vals[valskey + ".nid"] = self.nid
		vals[valskey + ".nmk"] = self.nmk
		
		
		if not ext.checkDone():
			debug("WARN: Not all bytes extracted in CM_SLAC_Match")

class MM_CM_Encrypted_Payload():
	def __init__(self, by, vals):
		ext = BytesExtractor(by)
		
		#MME
		#self.peksrsvd = bitsToBytes(ext.get(4))		#spec says "Four MSBs are 0x00"
		self.peks = ext.get(1)
		self.avlns = ext.get(1)
		self.pid = ext.get(1)
		self.prn = ext.get(2)
		self.pmn = ext.get(1)
		self.iv = ext.get(16)		#reversed?
		self.len = ext.get(2)		#reversed?
		self.length = int.from_bytes(self.len, byteorder='little', signed=False)

		self.encdata = ext.rest()
		self.data = None
		
		if not ext.checkDone():
			debug("WARN: Not all bytes extracted in CM_Encrypted_Payload")

			
class MM_CM_Get_Key():
	def __init__(self, by, vals):
		ext = BytesExtractor(by)
		
		self.encresult = ext.get(1)
		self.enckeytype = ext.get(1)
		self.encmynonce = ext.get(4)
		self.encyournonce = ext.get(4)
		self.encnid = ext.get(7)
		self.enceks = ext.get(1)
		self.encpid = ext.get(1)
		self.encprn = ext.get(2)
		self.encpmn = ext.get(1)
		self.enckey = ext.get(16)
		
		print("Encryp. Result: {}".format(self.encresult))
		print("Encryp. Key Type: {}".format(self.enckeytype))
		print("Encryp. My Nonce: {}".format(self.encmynonce))
		print("Encryp. Your Nonce: {}".format(self.encyournonce))
		print("Encryp. NID: {}".format(binascii.hexlify(self.encnid)))
		print("Encryp. EKS: {}".format(self.enceks))
		print("Encryp. PID: {}".format(self.encpid))
		print("Encryp. PRN: {}".format(self.encprn))
		print("Encryp. PMN: {}".format(self.encpmn))
		print("Encryp. Key: {}".format(self.enckey))

		if not ext.checkDone():
			debug("WARN: Not all bytes extracted in CM_Get_Key")