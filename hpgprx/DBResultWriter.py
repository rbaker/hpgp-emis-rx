from . import config

import sqlite3
import threading 
import time

from .utils import debug, warn, bitsToBytes
import binascii
import numpy as np


"""A class that asynchronously stores RX results in an SQLite3 DB. 
   It has a pretty weak thread-safety model: don't change results after they're in the queue. But that is fine for now."""
class DBResultWriter:
	def __init__(self, dbfile):
		self.dbfile = dbfile
		self.resultqueue = []
		
	def __enter__(self):
		self.stopthreads = False
		self.worker = threading.Thread(target=self.__storeResults)
		self.worker.start()
		
	def __exit__(self, exc_type, exc_val, exc_tb):
		debug("Shutting down DB worker thread")
		self.stopthreads = True
		self.worker.join(config.dbworkerfinalisetimeout)		#let it process the result of the queue, unless it takes too long
	
	def __initDBConnection(self, conn):
		c = conn.cursor()
		c.execute("create table if not exists processed (runid text, filenum integer, filename text, success integer, statusmsg text)")
		c.execute("create table if not exists debug (runid text, filenum integer, filename text, msgtype text, msg text)")
		c.execute("create table if not exists raw (runid text, filenum integer, filename text, obj text, bits blob, bytes blob, bytestext text)")
		c.execute("create table if not exists fc (runid text, filenum integer, filename text, crc24ok integer, desc text, fctype integer, access integer, snid blob)")
		c.execute("create table if not exists fcvariantfields (runid text, filenum integer, filename text, fields text)")
		c.execute("create table if not exists payload (runid text, filenum integer, filename text, crc32ok integer)")
		c.execute("create table if not exists beaconfields (runid text, filenum integer, filename text, fields text)")
		c.execute("create table if not exists beaconmngmtinfo (runid text, filenum integer, filename text, number integer, fields text)")
		#c.execute("create table if not exists managementmsg (runid text, filenum integer, filename text, )")
		c.execute("create table if not exists phyblocks (runid text, filenum integer, filename text, ssn integer, mfbo integer, vpbf blob, mmqf blob, mfbf integer, opsf blob, rsvd blob)")
		c.execute("create table if not exists macframe (runid text, filenum integer, filename text, mftype integer, mmtype blob, mmtypetext text, mmname text)")
		c.execute("create table if not exists macframevalues (runid text, filenum integer, filename text, key blob, val blob)")
		c.execute("create table if not exists keys (runid text, filenum integer, filename text, keyname text, keyvalue blob, keytext text)")
		conn.commit()
	
	"""External interface to store the results of a PPDU RX attempt"""
	def storeResult(self, dr, runid, filenum):
		self.resultqueue.append((dr, runid, filenum))
	
	"""The thread worker that stores all the results it has in the queue"""
	def __storeResults(self):
		self.conn = sqlite3.connect(self.dbfile)
		
		with self.conn:
			self.__initDBConnection(self.conn)

			while not (self.stopthreads and len(self.resultqueue) == 0):
				if len(self.resultqueue) > 0:
					debug("Storing result in DB")
					(dr, runid, filenum) = self.resultqueue.pop(0)

					try:
						c = self.conn.cursor()
						self.__doStore(dr, runid, filenum, c)
						self.conn.commit()
					except:										#still pretty bad error handling
						c = self.conn.cursor()
						self.__doStore(dr, runid, filenum, c)
						self.conn.commit()
				else:
					time.sleep(0.1)
				
	"""All the SQL for storing a result"""
	def __doStore(self, dr, runid, filenum, c):
		c.execute("insert into processed values (?, ?, ?, ?, ?)", (runid, filenum, dr.filename, dr.success, dr.statusmsg))
		if not dr.success and dr.trace is not None:
			c.execute("insert into debug values (?, ?, ?, ?, ?)", (runid, filenum, dr.filename, dr.statusmsg, dr.trace))
		if dr.success and not dr.ignore:
			c.execute("insert into raw values (?, ?, ?, ?, ?, ?, ?)", (runid, filenum, dr.filename, "FC", dr.ppdu.avfc.rawbits, bitsToBytes(dr.ppdu.avfc.rawbits), binascii.hexlify(bitsToBytes(dr.ppdu.avfc.rawbits))))
			c.execute("insert into fc values (?, ?, ?, ?, ?, ?, ?, ?)", (runid, filenum, dr.filename, dr.ppdu.avfc.isChecksumOk(), str(dr.ppdu.avfc), dr.ppdu.avfc.delimtype, dr.ppdu.avfc.access, dr.ppdu.avfc.snid))

			if dr.ppdu.avfc.delimtype == 0:		#beacon
				c.execute("insert into fcvariantfields values (?, ?, ?, ?)", (runid, filenum, dr.filename, str(dr.ppdu.avfc.variant.fields)))
			if dr.ppdu.avfc.delimtype == 1:		#sof
				c.execute("insert into fcvariantfields values (?, ?, ?, ?)", (runid, filenum, dr.filename, str(dr.ppdu.avfc.variant.fields)))

			if dr.ppdu.payload is not None:
				c.execute("insert into raw values (?, ?, ?, ?, ?, ?, ?)", (runid, filenum, dr.filename, "P", np.array(dr.ppdu.payload.rawbits), bitsToBytes(dr.ppdu.payload.rawbits), binascii.hexlify(bitsToBytes(dr.ppdu.payload.rawbits))))			#bit of a hack to convert the list to an array, but wevs
				c.execute("insert into payload values (?, ?, ?, ?)", (runid, filenum, dr.filename, dr.ppdu.payload.isChecksumOk()))

				if dr.ppdu.avfc.delimtype == 0:		#beacon
					c.execute("insert into beaconfields values (?, ?, ?, ?)", (runid, filenum, dr.filename, str(dr.ppdu.payload.fields)))

					if dr.ppdu.payload.isChecksumOk():		#otherwise the bmi extraction will likely fail
						c.execute("insert into beaconmngmtinfo values (?, ?, ?, ?, ?)", (runid, filenum, dr.filename, dr.ppdu.payload.bmi.nbe, str(dr.ppdu.payload.bmi.be)))

				if dr.ppdu.avfc.delimtype == 1:		#sof
					c.execute("insert into phyblocks values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", (runid, filenum, dr.filename, dr.ppdu.payload.ssn, dr.ppdu.payload.mfbo, dr.ppdu.payload.vpbf, dr.ppdu.payload.mmqf, dr.ppdu.payload.mfbf, dr.ppdu.payload.opsf, dr.ppdu.payload.rsvd))

			if dr.keys is not None:
				for (k, v) in dr.keys:
					c.execute("insert into keys values (?, ?, ?, ?, ?, ?)", (runid, filenum, dr.filename, k, v, binascii.hexlify(v)))

			if dr.mpdu is not None:
				if dr.mpdu.mftype == 3:
					c.execute("insert into macframe values (?, ?, ?, ?, ?, ?, ?)", (runid, filenum, dr.filename, dr.mpdu.mftype, dr.mpdu.mm.mmtype, str(hex(int.from_bytes(dr.mpdu.mm.mmtype, byteorder='little', signed=False))), dr.mpdu.mm.getMMName()))

					for v in dr.mpdu.mm.values:
						c.execute("insert into macframevalues values (?, ?, ?, ?, ?)", (runid, filenum, dr.filename, v, dr.mpdu.mm.values[v]))