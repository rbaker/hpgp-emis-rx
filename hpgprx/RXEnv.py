class RXEnv:
	def __init__(self):
		#self.noisevar = 0.00019	#initial estimate from the end of several PPDUs from Maldon		#clearly very incorrect from awful FEC performance
		#self.noisevar = 0.19		#arbitrary guess, but much better performance
		self.noisevar = 0.01		#arbitrary guess, but much better performance
		self.cpoestimate = 0
		self.residualcpos = []
		self.debugvals = {}		#a dictionary that things can be added to throughout
	
	def updateCPO(self, cpo):
		#self.cpo = cpo
		alpha = 0.9
		self.cpoestimate = alpha * self.cpoestimate + (1-alpha) * cpo
	
	def updateSCO(self, sco):
		self.sco = sco
	
	def updateChannel(self, channel):
		self.channel = channel