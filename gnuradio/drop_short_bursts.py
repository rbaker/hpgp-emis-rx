import os
import glob

MIN_SIZE = 200000

all_count = 0
del_count = 0

it = glob.glob("file[0-9]_*.dat")
for i in it:
	size = os.stat(i).st_size
	#print("{} : {} bytes".format(i, size))
	if size < MIN_SIZE:
		os.remove(i)
		del_count += 1
	all_count += 1

print("Processed {} files, deleted {}, kept {}".format(all_count, del_count, all_count - del_count))