## SDR Pre-processing Tools

The code in the main directory expects a series of individual packets (in a fairly round-the-houses format). The tools in this directory are intended to help in converting a raw signal capture to the appropriate format for processing. 

Nothing in the chain (either here or in the main directory) runs close to real-time, so the expected model for pre-processing is to capture a signal first and then process it offline. There are details of both stages below. 

There is also no filtering applied anywhere, so if there are interference sources nearby, it may be necessary to add an extra filtering step in between capture and packetising. 

These tools are very rudimentary, they were intended to work nearly out-of-the-box with just a stock GNURadio installation. It could be much easier with some out-of-tree blocks, but would involve the user understanding the OOT built process beforehand. 

## Setup

A few things need to be set up initially before using the tools. 

1. Open the Hier block in `DelayCorrelate.grc` in GNURadioCompanion and build it
1. Hit "Reload Blocks" in GNURadioCompanion (or close & re-open)
1. Open the main block in `RawToHPGPPkts.grc` and build it

Once the flowgraphs are built, they can be run from the command line, for ease of use. 

## Capture

Any raw IQ capture process can be used, provided it can meet the following criteria:

* Complex IQ samples, each 32-bit
* 30 Msps
* 15 MHz centre frequency

A very simple GNURadio flowgraph is given in `RawCapture.grc` that meets these criteria and should work with common SDRs (e.g., USRP N2xx, USRP B2xx, bladeRF etc.). 

At default speed (30 Msps, complex64) capturing a signal needs about 230 MB/s write speed, so most SSDs will handle it fine. 

If any additional filtering needs to be done on the raw signal, it can happen straight away. Removing interference can reduce the number of spurious packets output by the packetiser. 

## Pre-Processing

With a raw capture file prepared, run the packetiser:

	python -u RawToHPGPPkts.py --in-file=<your capture file>

The individual packets will be output to the working directory. Expect to get **a lot** of them. There is a convenience script to remove, from the working directory, any output files too small to be a packet:

	python drop_short_bursts.py 

This can be run after the packetising is complete or periodically during the packetiser run (e.g., if there is a noisy capture that is producing lots of files). 

The remaining files should be ready for processing in the main `receive.py` script from the top-level directory. 

## Notes

* Built and tested with GNURadio 3.7
* This isn't the method used for pre-processing in the paper. Based on my small-sample testing, these tools are much more permissive about what they consider a packet.
* If this is rubbish and you'd prefer a better version with OOT modules and custom code, do get in touch :)
* There are notes in the `RawToHPGPPkts.grc` file explaining the stages. Delay & Correlate is a common preamble-detection approach, the Burst Tagger is abused to annotate packets, the signals are interpolated up to HPGP sample rate (75 Msps, real-valued) and shifted to positive frequencies. The complex->real->complex conversion is a relic of how the main decoder is written, that I haven't got round to fixing yet. There are scopes in the flowgraph as well, that can be enabled to see the raw and trigger signals. 