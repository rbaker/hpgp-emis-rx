# HomePlug GreenPHY SDR RX

## Overview

This project is a software-based receiver for the HomePlug GP powerline communiction (PLC) standard. It is written in Python and was originally developed for a research project (see below) investigating wireless eavesdropping on PLC communication. 

A quick summary:

- Python 3 (+ numpy + scipy + matplotlib)
- Processes raw input files from any source (scope, SDR etc.)
- Handles complex IQ or real-valued input
- Receive-only
- Designed for HPGP, but will extract some data from HPAV networks
- Extracts full PHY data (headers + payloads)
- Decrypts PHY traffic when provided with an NMK/NEK
- Intercepts insecure SLAC key exchanges in ISO 15118
- Extracts MAC streams (poorly)
- Outputs V2GTP messages for use with V2GDecoder
- Software-only Turbo decoder for HPGP
- Far from real-time

Having control over the PHY layer reception lets us modify or use it in unusual ways (such as eavesdropping radiated emissions rather than connecting straight to the power cable). It also allows deeper observation of network behaviour than is currently available with hardware (see below). 

## Usage
The first thing to do is just run:

	python3 receive.py

That should process the signal captures in `example-data` and produce something in the `results` directory (along with a lot of debug to stdout). The results should be a .sqlite3 database and a .pcap file. 

One of the example files is a Beacon message from a HomePlugAV network in a house, the other is a MAC frame from an EV charging session (this particular message is part of the SLAC initialisation procedure and contains the NMK). When these are processed, the resulting database includes the data/metadata from the reception process for both packets, along with the NMK that was observed. The .pcap contains only the MAC traffic, which in this case is the one EV message. 

To process your own messages, just change the `inputdirs` in `receive.py` to a directory that contains your signal captures. The signals  **must** be separated into individual packets, otherwise only the first one will be processed (in GNURadio, a power trigger and tagged file sink should do the job). The packets should have a sample rate of 75MSPS, following the HPGP standard.

Deeper changes to processing can be made inside the `hpgprx/config.py` file, or by changing code. 

## Structure
The main things to interact with are:

- `SignalSources.py` : Objects offering several ways of reading in signal files
- `HPAVrx.py` : The top-level receiver, which coordinates the PHY and MAC layers inside it
- `DBResultWriter.py` : A quick hack to write results to an SQLite DB (asynchronously)
- `PPDU.py` and `MPDU.py` : Objects representing actual messages


## Notes

#### Advantages of PHY-layer Access
There has been quite a lot of work done with listening to HomePlug traffic using the *Sniffer Mode* on Qualcomm devices. This is by far the easier and cheaper way and often preferable to using a software-defined PHY-layer like this project. However, there are some notable limitations:

- No ability to control reception
- No access to message bodies (they aren't delivered by the sniffer)
- Some messages filtered (e.g., UKE exchanges)

None of those limitations apply if the reception is done in software. The downside is that we have to do all of the reception...

#### Key Recovery
The whole project was written to try to get keys out of the SLAC key exchange in DIN70121/ISO15118 EV charging sessions. As such the MAC layer constitutes **just** enough to achieve this and nothing more. If watching EV sessions then keys might occasionally be recovered into the database. Alternatively, if you already know the NMK/NEK for the network, you can set it inside `HPAVrxmac.py` and receive traffic. 

#### Receiving HPAV Traffic
Much of the PHY-layer is the same between HPAV and HPGP. However, a lot of settings have been removed in HPGP and this receiver has no support for them. The most notable difference is the use of robust broadcast, or "ROBO" modes. HPGP transmits everything in ROBO mode so it is comparatively easy to receive. HPAV transmits any unicast data using channel-optimised links, which are usually much harder to receive than ROBO broadcasts. 

#### Receiving HPAV2 Traffic
HPAV2 is clocked at a much higher rate than HPAV/HPGP. Much of the receiver could still work, but changes are needed throughout to get this to happen.

#### Performance
There is plenty of scope for performance improvement throughout the codebase. It was written for conceptual simplicity to begin with and very little has been done to make it fast. It is far away from being a real-time receiver like those in the GNURadio community. Nonetheless the turbo decoder dominates the runtime so speeding that up would help the most. 

Benchmarked on a laptop (Thinkpad X1 Carbon Gen 4, i7-6500U), it currently processes about 5 msgs/sec if it doesn't have to hit the turbo decoder. The turbo decoding is only applied as necessary (i.e., if a message validates its checksum then decoding stops early). Applying three turbo iterations on every message brings the throughput down to 0.3 msgs/sec. 

#### V2GDecoder/HomePlugPWN
Messages retrieved from a DIN 70121/ISO 15118 charging session will be in V2GTP format. Check out V2GDecoder (https://github.com/FlUxIuS/V2Gdecoder) and the rest of Seb Dudek's work to unwrap them further. 

There is currently no direct Scapy integration in this project, but if I need it, or hear that someone does, it seems quite achievable to add it. In the meantime, the extracted .pcap files can be read in manually. 

## Contact
I'd be delighted to talk about technical issues, bugs, or interesting uses of the project. Get in contact at: 
![](contactemail.png) 

## Licensing
This project is free software, provided under the MIT license. See license.txt. 

## Publications
This code was created as part of research into the use of PLC in EV charging via the CCS charging standard. If you're looking to cite us, the publication is:

>*Baker, R. and Martinovic, I., 2019. Losing the Car Keys: Wireless PHY-Layer Insecurity in EV Charging. In 28th USENIX Security Symposium (USENIX Security 19) (pp. 407-424).*